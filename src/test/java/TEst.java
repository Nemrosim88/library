import java.io.InputStreamReader;

import java.util.*;

import control.service.SendEmail;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class TEst {

    @Test
    public void getAllUsersTest() throws Exception {

        Properties properties = new Properties();
        properties.load(new InputStreamReader(
                ClassLoader.getSystemClassLoader().getResourceAsStream("i18n/text_ru.properties"), "UTF-8"));
//    Enumeration<?> enumeration = properties.propertyNames();
//    while(enumeration.hasMoreElements()){
//      enumeration.nextElement().toString();
//    }
//    System.out.println(properties.getProperty("doctor");

        System.out.println("========================");

        Locale locale = new Locale("us");
        ResourceBundle rb = ResourceBundle.getBundle("i18n.text", locale);

        Enumeration<String> keys = rb.getKeys();
        while (keys.hasMoreElements()) {
            System.out.println(keys.nextElement());
        }

    }

    @Test
    public void generator() {

        int length = 6;
        boolean useLetters = true;
        boolean useNumbers = true;
        String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);

        System.out.println(generatedString);
    }

    @Test
    public void mail() throws MessagingException {
        SendEmail.mail("nemrosim1988@gmail.com", "alibaba", "40");

    }

}
