package data.testUtils.dataBaseCreateAndFill;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;

import static data.testUtils.dao.abstraction.DAOUtils.readQuery;

public class LibraryDB implements DataBase {

  @Override
  public void createAndFill(Statement statement) {
    try {
      //      CREATE STATEMENTS
      statement.execute(readQuery("create\\schema.sql"));
      statement.execute(readQuery("create\\user.sql"));
      statement.execute(readQuery("create\\book.sql"));
      statement.execute(readQuery("create\\history.sql"));

      //      INSERT STATEMENTS
      statement.execute(readQuery("insert\\user.sql"));
      statement.execute(readQuery("insert\\book.sql"));
      statement.execute(readQuery("insert\\history.sql"));
    } catch (SQLException | IOException e) {
      e.printStackTrace();
    }
  }

}
