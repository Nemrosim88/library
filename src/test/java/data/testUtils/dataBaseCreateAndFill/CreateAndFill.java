package data.testUtils.dataBaseCreateAndFill;

import java.sql.Statement;

public class CreateAndFill {

  public CreateAndFill(DataBase dataBase, Statement statement) {
    dataBase.createAndFill(statement);
  }
}
