package data.testUtils.dao;

import entity.User;
import entity.enums.Role;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class UserTestUtils {

  /**
   * Метод возвращает всех пользователей без credentials and registration.
   */
  public static List<User> getListOfUsers() {
    List<User> list = new ArrayList<>();
    list.add(new User(1, "Виктор", "Викторов", "Викторович", Date.valueOf("1960-06-12"), Role.ADMIN, "АА123456", "12345","12345","hello@gmail.com", "+380631544182"));
    list.add(new User(2, "Андрей", "Спиридонов", "Викторович", Date.valueOf("1961-07-12"), Role.LIBRARIAN, "АА234432", "234234","234234","hello_world@gmail.com", "+380631554281"));
    return list;
  }

  public static User getUser() {
    User user = new User();
    user.setSurname("Петроновский");
    user.setName("Пётр");
    user.setPatronymic("Васильевич");
    user.setDayOfBirth(Date.valueOf("1988-05-10"));
    user.setRole(Role.READER);
    return user;
  }

}
