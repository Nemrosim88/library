package data.testUtils.dao.abstraction;

import entity.abstraction.Entity;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public interface DAOUtils<E extends Entity> {

  /**
   * Method reads sql commands from files.
   * @param fileName
   * @return
   * @throws IOException
   */
  static String readQuery(String fileName) throws IOException {
    String path = "\\src\\main\\resources\\sql\\";
    String absolutePath = new File("").getAbsolutePath();

    String pathToFile = absolutePath + path;

    StringBuilder sb = new StringBuilder();
    Files.readAllLines(Paths.get(pathToFile + fileName)).forEach(sb::append);
    return sb.toString();
  }

  E getNew();

  List<E> getAll();

}
