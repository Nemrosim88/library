package data.testUtils.dao;

import entity.History;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class HistoryTestUtils {

    /**
     * Метод возвращает все сущности history из базы.
     */
    public static List<History> getListOfHistory() {
        List<History> list = new ArrayList<>();
        list.add(new History(1, 1, 2,
                Timestamp.valueOf("2009-06-04 18:13:56"),
                Timestamp.valueOf("2009-06-04 18:15:56"),
                "helloWorld"));
        list.add(new History(2, 1, 2,
                Timestamp.valueOf("2009-06-04 19:13:56"),
                Timestamp.valueOf("2009-06-04 20:15:56"),
                "hello-World"));
        return list;
    }

    /**
     * Возвращает новую сущность history.
     * @return
     */
    public static History getNewHistory() {
        History history = new History();
        history.setBookId(1);
        history.setUserId(2);
        history.setDateOfIssue(Timestamp.valueOf("2009-07-05 20:13:56"));
        history.setReturnDate(Timestamp.valueOf("2009-08-06 15:13:56"));
        history.setInfo("Hello_World!");
        return history;
    }
}
