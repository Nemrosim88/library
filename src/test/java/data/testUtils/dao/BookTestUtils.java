package data.testUtils.dao;

import entity.Book;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class BookTestUtils {


    /**
     * Метод возвращает всех пользователей без credentials and registration.
     */
    public static List<Book> getListOfBooks() {
        List<Book> list = new ArrayList<>();
        list.add(new Book(1, "Артемида", "Артемида – единственный город на Луне.",
                "Зарубежная фантастика",
                "Энди",
                "Вейер",
                "",
                Date.valueOf("2017-01-01"),
                10,
                0,
                ""));
        list.add(new Book(2, "Sapiens. Краткая история человечества",
                "Сто тысяч лет назад Homo sapiens был одним из как минимум шести видов человека.",
                "Научно-популярная литература",
                "Юваль",
                "Ной",
                "Харари",
                Date.valueOf("2011-01-01"),
                15,
                0,
                ""));

        list.add(new Book(3,
                "Гормоны счастья. Как приучить мозг вырабатывать серотонин, дофамин, эндорфин и окситоцин",
                "Автор этой книги предлагает узнать все о работе гормонов.",
                "Научно-популярная литература",
                "Лоретта",
                "Бройнинг",
                "",
                Date.valueOf("2016-01-01"),
                12,
                0,
                ""));

        return list;
    }

    public static Book getBook() {
        Book book = new Book();
        book.setName("Текст");
        book.setInfo("«Текст» – первый реалистический роман Дмитрия Глуховского.");
        book.setGenre("Современная русская литература");
        book.setAuthorName("Дмитрий");
        book.setAuthorSurname("Глуховский");
        book.setAuthorPatronymic("");
        book.setYearOfEdition(Date.valueOf("2017-01-01"));
        book.setAmount(16);
        book.setIssued(0);
        book.setImg_href("");
        return book;
    }
}
