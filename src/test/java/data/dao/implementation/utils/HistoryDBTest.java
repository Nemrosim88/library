package data.dao.implementation.utils;

import data.Constants;
import data.dao.implementation.EntityDAO;
import data.testUtils.dao.HistoryTestUtils;
import data.testUtils.dataBaseCreateAndFill.CreateAndFill;
import data.testUtils.dataBaseCreateAndFill.LibraryDB;
import entity.History;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.*;
import org.junit.rules.Timeout;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class HistoryDBTest {

    @Rule
    public final Timeout timeout = Timeout.seconds(3);

    private static Connection connection;
    private static Statement statement;
    private static EntityDAO<History> entityDAO;

    @Test
    public void getAllUsersTest() {
        List<History> expected = HistoryTestUtils.getListOfHistory();
        List<History> result = entityDAO.getAll();
        assertThat(expected, is(result));
    }

    @Test
    public void getByKeyTest() {
        List<History> expected = new ArrayList<>();

        History entity = HistoryTestUtils.getListOfHistory().get(0);
        expected.add(entity);
        List<History> result = entityDAO.getByKey(Constants.HISTORY_ID_COLUMN_NAME, entity.getId());
        assertThat(expected, is(result));
    }

    @Test
    public void getByBookIdTest() {
        List<History> expected = HistoryTestUtils.getListOfHistory();
        List<History> result = entityDAO.getByKey(Constants.HISTORY_BOOK_ID_COLUMN_NAME, 1);
        assertThat(expected, is(result));
    }

    @Test
    public void getByUserIdTest() {
        List<History> expected = HistoryTestUtils.getListOfHistory();
        List<History> result = entityDAO.getByKey(Constants.HISTORY_USER_ID_COLUMN_NAME, 2);
        assertThat(expected, is(result));
    }

    @Test
    public void getByValueInfoTest() {
        List<History> expected = new ArrayList<>();
        expected.add(HistoryTestUtils.getListOfHistory().get(0));
        List<History> result = entityDAO.getByValue(Constants.HISTORY_INFO_COLUMN_NAME,
                "helloWorld");
        assertThat(result, is(expected));
    }


    @Test
    public void insertTest() {
        History entity = HistoryTestUtils.getNewHistory();
        int expected = entityDAO.insert(entity);
        assertTrue(expected == 3);
    }

    @Test
    public void isExistTest() {
        History expected = HistoryTestUtils.getListOfHistory().get(0);
        History result = entityDAO.isExist(expected);
        assertTrue(result != null);
    }

    @Test
    public void updateTest() throws SQLException {
        List<History> expectedList = new ArrayList<>();
        History entity = HistoryTestUtils.getListOfHistory().get(0);
        entity.setBookId(1);
        entity.setInfo("World");
        entity.setReturnDate(Timestamp.valueOf("2009-06-04 20:15:56"));
        expectedList.add(entity);

        boolean successfullyUpdated = entityDAO.update(entity);
        assertTrue(successfullyUpdated);

        List<History> expectedBookAfterUpdate = entityDAO.getByKey(Constants.BOOK_ID_COLUMN_NAME, entity.getId());
        assertThat(expectedList, is(expectedBookAfterUpdate));
    }

    @Test
    public void deleteTest() throws SQLException {
        List<History> expectedList = new ArrayList<>();
        History entity = HistoryTestUtils.getListOfHistory().get(0);
        expectedList.add(entity);

        int entityId = entity.getId();

        boolean expected = entityDAO.delete(entityId);
        assertTrue(expected);

        List<History> expectedUserAfterDelete = entityDAO.getByKey(Constants.BOOK_ID_COLUMN_NAME, entityId);


        // Проверка на CASCADE SQL таблиц
        assertTrue(expectedUserAfterDelete.isEmpty());
    }

    @BeforeClass
    public static void setUp() throws Exception {
        ResourceBundle resource = ResourceBundle.getBundle("database");
        Class.forName(resource.getString("H2driver"));
        DataSource dataSource = JdbcConnectionPool.create(
                resource.getString("H2url"),
                resource.getString("H2user"),
                resource.getString("H2password")
        );
        connection = dataSource.getConnection();
        entityDAO = new EntityDAO<>(connection, HistoryUtils.getInstance());
    }

    @Before
    public void setConnection() throws SQLException {
        statement = connection.createStatement();
        new CreateAndFill(new LibraryDB(), statement);
        statement.close();
    }

    @After
    public void closeConnection() throws SQLException {
        statement = connection.createStatement();
        statement.execute("DROP SCHEMA library");
        statement.close();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        connection.close();
        assertTrue(connection.isClosed());
        assertTrue(statement.isClosed());
    }
}
