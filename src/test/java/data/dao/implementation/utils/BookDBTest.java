package data.dao.implementation.utils;

import data.Constants;
import data.dao.implementation.EntityDAO;
import data.testUtils.dao.BookTestUtils;
import data.testUtils.dataBaseCreateAndFill.CreateAndFill;
import data.testUtils.dataBaseCreateAndFill.LibraryDB;
import entity.Book;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.*;
import org.junit.rules.Timeout;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class BookDBTest {

    @Rule
    public final Timeout timeout = Timeout.seconds(3);

    private static Connection connection;
    private static Statement statement;
    private static EntityDAO<Book> entityDAO;

    @Test
    public void getAllUsersTest() {
        List<Book> expected = BookTestUtils.getListOfBooks();
        List<Book> result = entityDAO.getAll();
        assertThat(expected, is(result));
    }

    @Test
    public void getByKeyTest() {
        List<Book> expected = new ArrayList<>();

        Book entity = BookTestUtils.getListOfBooks().get(0);
        expected.add(entity);
        List<Book> result = entityDAO.getByKey(Constants.BOOK_ID_COLUMN_NAME, entity.getId());
        assertThat(expected, is(result));
    }

    @Test
    public void getByValueBookNameTest() {
        List<Book> expected = new ArrayList<>();
        expected.add(BookTestUtils.getListOfBooks().get(1));
        List<Book> result = entityDAO.getByValue(Constants.BOOK_NAME_COLUMN_NAME,
                "Sapiens. Краткая история человечества");
        assertThat(result, is(expected));
    }

    @Test
    public void getByValueAuthorSurnameTest() {
        List<Book> expected = new ArrayList<>();
        expected.add(BookTestUtils.getListOfBooks().get(0));

        List<Book> result = entityDAO.getByValue(Constants.BOOK_AUTHOR_SURNAME_COLUMN_NAME,
                "Вейер");
        assertThat(result, is(expected));
    }


    @Test
    public void insertTest() {
        Book entity = BookTestUtils.getBook();
        int expected = entityDAO.insert(entity);
        assertTrue(expected == 4);
    }

    @Test
    public void isExistTest() {
        Book expected = BookTestUtils.getListOfBooks().get(0);
        Book result = entityDAO.isExist(expected);
        assertTrue(result != null);
    }

    @Test
    public void updateTest() throws SQLException {
        List<Book> expectedList = new ArrayList<>();
        Book entity = BookTestUtils.getListOfBooks().get(0);
        entity.setName("Hello");
        entity.setInfo("World");
        entity.setYearOfEdition(Date.valueOf("2012-12-12"));
        expectedList.add(entity);

        boolean successfullyUpdated = entityDAO.update(entity);
        assertTrue(successfullyUpdated);

        List<Book> expectedBookAfterUpdate = entityDAO.getByKey(Constants.BOOK_ID_COLUMN_NAME, entity.getId());
        assertThat(expectedList, is(expectedBookAfterUpdate));
    }

    /**
     * TODO удаление книги? серьезно?
     * @throws SQLException
     */
    @Test
    public void deleteTest() throws SQLException {
        List<Book> expectedList = new ArrayList<>();
        Book entity = BookTestUtils.getListOfBooks().get(0);
        expectedList.add(entity);

        int entityId = entity.getId();

        boolean expected = entityDAO.delete(entityId);
        assertTrue(expected);

        List<Book> expectedUserAfterDelete = entityDAO.getByKey(Constants.BOOK_ID_COLUMN_NAME, entityId);


        // Проверка на CASCADE SQL таблиц
        assertTrue(expectedUserAfterDelete.isEmpty());
    }


    @BeforeClass
    public static void setUp() throws Exception {
        ResourceBundle resource = ResourceBundle.getBundle("database");
        Class.forName(resource.getString("H2driver"));
        DataSource dataSource = JdbcConnectionPool.create(
                resource.getString("H2url"),
                resource.getString("H2user"),
                resource.getString("H2password")
        );
        connection = dataSource.getConnection();
        entityDAO = new EntityDAO<>(connection, BookUtils.getInstance());
    }

    @Before
    public void setConnection() throws SQLException {
        statement = connection.createStatement();
        new CreateAndFill(new LibraryDB(), statement);
        statement.close();
    }


    @After
    public void closeConnection() throws SQLException {
        statement = connection.createStatement();
        statement.execute("DROP SCHEMA library");
        statement.close();
    }


    @AfterClass
    public static void tearDown() throws Exception {
        connection.close();
        assertTrue(connection.isClosed());
        assertTrue(statement.isClosed());
    }


}
