package data.dao;

import data.dao.implementation.utils.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@Suite.SuiteClasses(
        {
                UserDBTest.class,
                BookDBTest.class,
                HistoryDBTest.class
//        CreateANDFillMariaDB.class
        }
)
@RunWith(Suite.class)
public class DAOTestSuite {

}
