<%@page contentType="text/html" pageEncoding="utf-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<jsp:useBean id="properties" class="java.util.Properties" scope="session"/>--%>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<c:set var="mainPageName" value="${properties.index_page_name}"/>


<!DOCTYPE html>
<html lang="${properties.language}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Artem Diashkin">

    <title>${mainPageName}</title>

    <link rel="icon" href="${context}/img/main_icon.png">
    <link rel="stylesheet" href="${context}/css/bootstrap.min.css">
</head>

<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">

    <a class="navbar-brand" href="/">
        <img src="${context}/img/main_icon.png"
             title="${properties.home}"
             width="40"
             height="40">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">

        <jsp:include page="${context}/WEB-INF/jsp/includes/nav.jsp">
            <jsp:param name="form_class" value="navbar-nav mr-auto"/>
        </jsp:include>

        <jsp:include page="${context}/WEB-INF/jsp/includes/languageProperties.jsp">
            <jsp:param name="this_page" value="viewHistory"/>
            <jsp:param name="image_size" value="25"/>
            <jsp:param name="command" value="getProperties"/>
            <jsp:param name="form_class" value="form-inline mt-2 mt-md-0"/>
        </jsp:include>


        <jsp:include page="${context}/WEB-INF/jsp/includes/loginForm.jsp">
            <jsp:param name="command" value="login"/>
            <jsp:param name="form_class" value="form-control mr-sm-2"/>
        </jsp:include>

    </div>


</nav>

<main role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <div class="media">
                <img class="mr-lg-2" src="${context}/img/icons8-literature-64.png"
                     alt="Add new user image">
                <div class="media-body">
                    <h1 class="display-3">Просмотр всей истории выдачи и возврата книг</h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container">

        <%--@elvariable id="historyList" type="java.util.List"--%>
        <c:choose>
            <c:when test="${empty historyList}">
                <form class="form-inline mt-2 mt-md-0" action="controller" method="post">
                    <input type="hidden" name="command" value="viewHistory">
                    <button class="btn btn-primary btn-lg btn-block"
                            type="submit">${properties.view_history}</button>
                </form>
                <br>
            </c:when>
        </c:choose>

        <jsp:include page="${context}/WEB-INF/jsp/includes/viewHistory/viewHistory.jsp">
            <jsp:param name="div_class" value="input-group mb-3"/>
            <jsp:param name="input_class" value="form-control"/>
            <jsp:param name="form_class" value="form-inline mt-2 mt-md-0"/>
        </jsp:include>

    </div> <!-- /container -->

</main>

<footer class="container">
    <p>&copy; Company 2018</p>
</footer>

<script src="${context}/js/jquery-3.3.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
        integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
        integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
        crossorigin="anonymous"></script>

</body>

</html>