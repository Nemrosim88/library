<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<c:set var="context" value="${pageContext.request.contextPath}"/>

<c:choose>
    <%--@elvariable id="user" type="entity.User"--%>
    <c:when test="${user != null}">
        <c:import url="${context}/WEB-INF/jsp/includes/logoutForm.jsp"/>
    </c:when>
    <c:otherwise>
        <%--@elvariable id="message" type="java.lang.String"--%>
        <label style="color: red">${message}&ensp;</label>
        <form class="form-inline my-2 my-lg-0" action="controller" method="post">
            <input type="hidden" name="command" value="login">
            <c:choose>
                <%--@elvariable id="login" type="java.lang.String"--%>
                <c:when test="${login != null}">
                    <input type="text"
                           name="login"
                           class="${param.form_class}"
                           style="background-color: #f8d7da"
                           placeholder="${properties.login}"
                           value="${login}"
                           aria-label="Search">
                    <input class="${param.form_class}"
                           type="password"
                           name="password"
                           style="background-color: #f8d7da"
                           placeholder="${properties.password}"
                           aria-label="Password">
                </c:when>
                <c:otherwise>
                    <input class="${param.form_class}"
                           type="text"
                           name="login"
                           placeholder="${not empty cookie.cookie_login ?
                               cookie.cookie_login.value:
                                properties.login}"
                           aria-label="Search">
                    <input class="${param.form_class}"
                           type="password"
                           name="password"
                           placeholder="${properties.password}"
                           aria-label="Password">
                </c:otherwise>
            </c:choose>
            <button class="btn btn-outline-success my-2 my-sm-0"
                    type="submit">${properties.in}</button>
        </form>
    </c:otherwise>
</c:choose>
