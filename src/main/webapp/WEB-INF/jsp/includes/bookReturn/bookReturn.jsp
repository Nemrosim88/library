<%--@elvariable id="historyId" type="java.lang.Integer"--%>
<%--@elvariable id="info" type="java.lang.String"--%>
<%--@elvariable id="bookId" type="java.lang.Integer"--%>
<%--@elvariable id="userId" type="java.lang.Integer"--%>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@page contentType="text/html" pageEncoding="utf-8" language="java" %>


<label>
    <b>
        ${not empty bookId ?  'Книга с ID '+ bookId
                + ' успешно возвращена и запись истории обновлена: ' + historyId : ''}
    </b>
</label>
<label style="color: ${not empty errorMessage ? 'red' : 'black'}">
    <%--@elvariable id="bookMessage" type="java.lang.String"--%>
    ${not empty bookMessage ? bookMessage : ''}
</label>

<form action="controller" method="post">
    <input type="hidden" name="command" value="${param.command}">

    <div class="${param.div_class}">
        <input type="number" class="${param.input_class}"
               placeholder="${properties.history_id}" name="historyId"
               aria-label="historyId" aria-describedby="basic-addon2"
               maxlength="20" value="${historyId}" required>
    </div>


    <button type="submit"
            class="btn btn-primary btn-lg btn-block">${properties.return_book}</button>
</form>
