<%--@elvariable id="user" type="entity.User"--%>
<%--@elvariable id="userID" type="java.lang.Integer"--%>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@page contentType="text/html" pageEncoding="utf-8" language="java" %>
<%@ page import="entity.enums.Role" %>


<label>
    <b>
        ${not empty userID ?  'Новому пользователю присвоено ID:' : ''}
        &ensp;
        ${not empty userID ?  userID : ''}
    </b>
</label>
<label style="color: ${not empty errorMessage ? 'red' : 'black'}">
    <%--@elvariable id="errorMessage" type="java.lang.String"--%>
    ${not empty errorMessage ? errorMessage : ''}
</label>

<form action="controller" method="post">
    <input type="hidden" name="command" value="${param.command}">

    <div class="${param.div_class}">
        <input type="text" class="${param.input_class}"
               placeholder="${properties.name}" name="name"
               aria-label="name" aria-describedby="basic-addon2"
               maxlength="20"
               value="${user.name}" required>
        <div class="input-group-prepend">
            <label class="input-group-text" for="inputGroupSelect01">${properties.role}</label>
        </div>
        <select class="custom-select" id="inputGroupSelect01" name="role" required>
            <option value="admin" ${user.role == Role.ADMIN ? 'selected' : ''}>
                ${properties.admin}
            </option>
            <option value="librarian" ${user.role == Role.LIBRARIAN ? 'selected' : ''}>
                ${properties.librarian}
            </option>
            <option value="reader" ${user.role == Role.READER ? 'selected' : ''}>
                ${properties.reader}
            </option>
        </select>
    </div>

    <div class="${param.div_class}">
        <input type="text" class="${param.input_class}"
               placeholder="${properties.surname}" name="surname"
               aria-label="surname" aria-describedby="basic-addon2"
               maxlength="20"
               value="${user.surname}" required>
    </div>


    <div class="${param.div_class}">
        <input type="text" class="${param.input_class}"
               placeholder="${properties.patronymic}" name="patronymic"
               aria-label="patronymic" aria-describedby="basic-addon2"
               maxlength="20"
               value="${user.patronymic}" required>
    </div>

    <div class="container">
        ${properties.day_of_birth}
    </div>
    <div class="${param.div_class}">
        <input type="date" class="${param.input_class}"
               placeholder="03.04.1940" name="dayOfBirth"
               aria-label="day_of_birth" aria-describedby="basic-addon2"
               value="${user.dayOfBirth}" required>
    </div>


    <div class="${param.div_class}">
        <input type="text" class="${param.input_class}"
               placeholder="${properties.passport}" name="passport" maxlength="8"
               aria-label="passport" aria-describedby="basic-addon2"
               value="${user.passport}" required>
    </div>


    <div class="${param.div_class}">
        <input type="text" class="${param.input_class}"
               placeholder="${properties.login}*" name="login"
               aria-label="login" aria-describedby="basic-addon2"
               maxlength="45"
               value="${user.login}" required>
    </div>

    <div class="container">
        Пароль будет сгенерирован автоматически и отправлен на почту
    </div>

    <div class="${param.div_class}">
        <input type="email" class="${param.input_class}"
               placeholder="${properties.email}" name="email"
               aria-label="email" aria-describedby="basic-addon2"
               maxlength="45"
               value="${user.email}" required>
    </div>

    <div class="${param.div_class}">
        <input type="number" class="${param.input_class}"
               placeholder="380671112233" name="phoneNumber"
               aria-label="phone_number" aria-describedby="basic-addon2"
               maxlength="16"
               value="${user.phoneNumber}" required>
    </div>


    <button type="submit"
            class="btn btn-primary btn-lg btn-block">${properties.add_new_user}</button>
</form>
