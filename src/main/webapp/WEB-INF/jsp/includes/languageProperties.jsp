<label>&ensp;</label>
<form class="${param.form_class}" action="controller" method="post">
    <input type="hidden" name="jspPage" value="${param.this_page}">
    <input type="hidden" name="command" value="${param.command}">
    <input type="hidden" name="language" value="us">
    <input type="image" src="https://png.icons8.com/usa/office/40"
           width="${param.image_size}" height="${param.image_size}"
           alt="US"/>
</form>
<label>&ensp;</label>
<form class="${param.form_class}" action="controller" method="post">
    <input type="hidden" name="jspPage" value="${param.this_page}">
    <input type="hidden" name="command" value="${param.command}">
    <input type="hidden" name="language" value="ru">
    <input type="image" src="https://png.icons8.com/russian-federation/office/40"
           width="${param.image_size}" height="${param.image_size}"
           alt="RU"/>
</form>
<label>&ensp;</label>
<form class="${param.form_class}" action="controller" method="post">
    <input type="hidden" name="jspPage" value="${param.this_page}">
    <input type="hidden" name="command" value="${param.command}">
    <input type="hidden" name="language" value="uk">
    <input type="image" src="https://png.icons8.com/ukraine/office/40"
           width="${param.image_size}" height="${param.image_size}"
           alt="UK"/>
</form>
<label>&ensp;</label>
<form class="${param.form_class}" action="controller" method="post">
    <input type="hidden" name="jspPage" value="${param.this_page}">
    <input type="hidden" name="command" value="${param.command}">
    <input type="hidden" name="language" value="fr">
    <input type="image" src="https://png.icons8.com/france/office/40"
           width="${param.image_size}" height="${param.image_size}"
           alt="FR"/>
</form>
