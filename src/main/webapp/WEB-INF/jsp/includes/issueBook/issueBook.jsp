<%--@elvariable id="info" type="java.lang.String"--%>
<%--@elvariable id="bookId" type="java.lang.Integer"--%>
<%--@elvariable id="userId" type="java.lang.Integer"--%>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@page contentType="text/html" pageEncoding="utf-8" language="java" %>


<label>
    <b>
        ${not empty bookId ?  'Книга с таким ID успешно выдана и запись внесена в базу: ' : ''}
        &ensp;
        ${not empty bookId ?  bookId : ''}
    </b>
</label>
<<br>
<label style="color: ${not empty errorMessage ? 'red' : 'black'}">
    <%--@elvariable id="bookMessage" type="java.lang.String"--%>
    ${not empty bookMessage ? bookMessage : ''}
</label>

<form action="controller" method="post">
    <input type="hidden" name="command" value="${param.command}">

    <div class="${param.div_class}">
        <input type="number" class="${param.input_class}"
               placeholder="${properties.book_id}" name="bookId"
               aria-label="bookId" aria-describedby="basic-addon2"
               maxlength="20" value="${bookId}" required>
    </div>

    <div class="${param.div_class}">
        <input type="number" class="${param.input_class}"
               placeholder="${properties.user_id}" name="userId"
               aria-label="userId" aria-describedby="basic-addon2"
               maxlength="20" value="${userId}" required>
    </div>

    <div class="${param.div_class}">
        <textarea type="text" class="${param.input_class}" rows="4"
                  placeholder="${properties.info}" name="info"
                  aria-label="info" aria-describedby="basic-addon2"
                  maxlength="250" required> ${info}</textarea>
    </div>


    <button type="submit"
            class="btn btn-primary btn-lg btn-block">${properties.issue_book}</button>
</form>
