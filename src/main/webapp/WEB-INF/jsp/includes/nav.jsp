<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="entity.enums.Role" %>
<%--@elvariable id="properties" type="java.util.Properties"--%>


<ul class="${param.form_class}">

    <li class="${param.li_class}">
        <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01"
           data-toggle="dropdown" aria-haspopup="true"
           aria-expanded="false">${properties.menu}</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">


            <c:choose>
                <%--@elvariable id="logged" type="entity.enums.Role"--%>
                <c:when test="${logged eq Role.ADMIN}">
                    <c:set var="css_div_class" value="alert alert-success"/>

                    <div class="${css_div_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=addNewUsers">${properties.add_new_user}</a>
                    </div>

                    <div class="${css_div_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=addNewBook">Добавить новую
                            книгу</a>
                    </div>

                    <div class="${css_div_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=bookReturn">Вернуть
                            книгу</a>
                    </div>

                    <div class="${css_div_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=viewHistory">Просмотреть
                            историю</a>
                    </div>

                    <div class="${css_div_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=viewAllBooks">Просмотреть
                            все книги</a>
                    </div>

                    <div class="${css_div_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=issueBook">Выписать
                            книгу</a>
                    </div>

                </c:when>


                <c:when test="${logged eq Role.READER}">
                    <c:set var="css_div_alert_class" value="alert alert-danger"/>
                    <c:set var="css_div_success_class" value="alert alert-success"/>

                    <div class="${css_div_alert_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=addNewUsers">Добавить нового
                            пользователя</a>
                    </div>

                    <div class="${css_div_alert_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=addNewBook">Добавить новую
                            книгу</a>
                    </div>

                    <div class="${css_div_alert_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=bookReturn">Вернуть
                            книгу</a>
                    </div>

                    <div class="${css_div_alert_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=viewHistory">Просмотреть
                            историю</a>
                    </div>

                    <div class="${css_div_success_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=viewAllBooks">Просмотреть
                            все книги</a>
                    </div>

                    <div class="${css_div_alert_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=issueBook">Выписать
                            книгу</a>
                    </div>

                </c:when>


                <c:when test="${logged eq Role.LIBRARIAN}">
                    <c:set var="css_div_alert_class" value="alert alert-danger"/>
                    <c:set var="css_div_success_class" value="alert alert-success"/>

                    <div class="${css_div_alert_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=addNewUsers">Добавить нового
                            пользователя</a>
                    </div>

                    <div class="${css_div_success_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=addNewBook">Добавить новую
                            книгу</a>
                    </div>

                    <div class="${css_div_success_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=bookReturn">Вернуть
                            книгу</a>
                    </div>

                    <div class="${css_div_success_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=viewHistory">Просмотреть
                            историю</a>
                    </div>

                    <div class="${css_div_success_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=viewAllBooks">Просмотреть
                            все книги</a>
                    </div>

                    <div class="${css_div_success_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=issueBook">Выписать
                            книгу</a>
                    </div>

                </c:when>


                <c:otherwise>
                    <c:set var="css_div_alert_class" value="alert alert-danger"/>
                    <c:set var="css_div_success_class" value="alert alert-success"/>

                    <div class="${css_div_alert_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=addNewUsers">Добавить нового
                            пользователя</a>
                    </div>

                    <div class="${css_div_alert_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=addNewBook">Добавить новую
                            книгу</a>
                    </div>

                    <div class="${css_div_alert_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=bookReturn">Вернуть
                            книгу</a>
                    </div>

                    <div class="${css_div_alert_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=viewHistory">Просмотреть
                            историю</a>
                    </div>

                    <div class="${css_div_success_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=viewAllBooks">Просмотреть
                            все книги</a>
                    </div>

                    <div class="${css_div_alert_class}" role="alert">
                        <a class="dropdown-item" href="/controller?page=issueBook">Выписать
                            книгу</a>
                    </div>
                </c:otherwise>
            </c:choose>


    </li>
</ul>
