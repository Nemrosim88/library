<%--@elvariable id="properties" type="java.util.Properties"--%>
<%--@elvariable id="cssProperties" type="java.util.Properties"--%>
<%@ page import="entity.History" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>


<%--@elvariable id="historyList" type="java.util.List"--%>
<c:forEach items="${historyList}" var="history">
    <label>${properties.history_id} : ${history.id}</label>
    <table class="table table-sm">

        <thead>
        <tr>
            <th scope="col">${properties.user_id}</th>
            <th scope="col">${properties.surname}</th>
            <th scope="col">${properties.name}</th>
            <th scope="col">${properties.patronymic}</th>
            <th scope="col">${properties.passport}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">${history.user.id}</th>
            <td>${history.user.surname}</td>
            <td>${history.user.name}</td>
            <td>${history.user.patronymic}</td>
            <td>${history.user.passport}</td>
        </tr>
        </tbody>

        <thead>
        <tr>
            <th scope="col">${properties.book_id}</th>
            <th scope="col">${properties.name}</th>
            <th scope="col">${properties.authorSurname}</th>
            <th scope="col">${properties.date_of_issue}</th>
            <th scope="col">${properties.return_date}</th>
        </tr>
        </thead>
        <tbody>


        <c:choose>
        <c:when test="${not empty history.returnDate}">
        <tr class="table-success">
            </c:when>
            <c:otherwise>
        <tr class="table-danger">
            </c:otherwise>
            </c:choose>

            <th scope="row">${history.book.id}</th>
            <td>${history.book.name}</td>
            <td>${history.book.authorSurname}</td>

            <fmt:formatDate value="${history.dateOfIssue}" var="issueDate"
                            type="date" pattern="yyyy.MM.dd HH:mm"/>
            <td>${issueDate}</td>

            <fmt:formatDate value="${history.returnDate}" var="returnDate"
                            type="date" pattern="yyyy.MM.dd HH:mm"/>
            <td>${returnDate}</td>
        </tr>
        </tbody>
    </table>
    <br>
</c:forEach>
