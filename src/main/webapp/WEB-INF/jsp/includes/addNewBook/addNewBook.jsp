<%--@elvariable id="book" type="entity.Book"--%>
<%--@elvariable id="bookID" type="java.lang.Integer"--%>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@page contentType="text/html" pageEncoding="utf-8" language="java" %>
<%@ page import="entity.enums.Role" %>


<label>
    <b>
        ${not empty bookID ?  'Новой книге присвоено ID:' : ''}
        &ensp;
        ${not empty bookID ?  bookID : ''}
    </b>
</label>
<label style="color: ${not empty errorMessage ? 'red' : 'black'}">
    <%--@elvariable id="errorMessage" type="java.lang.String"--%>
    ${not empty errorMessage ? errorMessage : ''}
</label>

<form action="controller" method="post">
    <input type="hidden" name="command" value="${param.command}">

    <div class="${param.div_class}">
        <input type="text" class="${param.input_class}"
               placeholder="${properties.name}" name="name"
               aria-label="name" aria-describedby="basic-addon2"
               maxlength="300"
               value="${book.name}" required>
    </div>

    <div class="${param.div_class}">
        <textarea type="text" class="${param.input_class}" rows="3"
                  placeholder="${properties.info}" name="info"
                  aria-label="surname" aria-describedby="basic-addon2"
                  maxlength="3000" required> ${book.info}</textarea>
    </div>


    <div class="${param.div_class}">
        <input type="text" class="${param.input_class}"
               placeholder="${properties.genre}" name="genre"
               aria-label="patronymic" aria-describedby="basic-addon2"
               maxlength="45"
               value="${book.genre}" required>
    </div>

    <div class="${param.div_class}">
        <input type="text" class="${param.input_class}"
               placeholder="${properties.authorName}" name="authorName"
               aria-label="patronymic" aria-describedby="basic-addon2"
               maxlength="45"
               value="${book.authorName}" required>
    </div>


    <div class="${param.div_class}">
        <input type="text" class="${param.input_class}"
               placeholder="${properties.authorSurname}" name="authorSurname"
               aria-label="patronymic" aria-describedby="basic-addon2"
               maxlength="45"
               value="${book.authorSurname}" required>
    </div>

    <div class="${param.div_class}">
        <input type="text" class="${param.input_class}"
               placeholder="${properties.authorPatronymic}" name="authorPatronymic"
               aria-label="patronymic" aria-describedby="basic-addon2"
               maxlength="45"
               value="${book.authorPatronymic}">
    </div>

    <div class="container">
        ${properties.yearOfEdition}
    </div>
    <div class="${param.div_class}">
        <input type="date" class="${param.input_class}"
               placeholder="03.04.1940" name="yearOfEdition"
               aria-label="day_of_birth" aria-describedby="basic-addon2"
               value="${book.yearOfEdition}" required>
    </div>


    <div class="${param.div_class}">
        <input type="number" class="${param.input_class}"
               placeholder="${properties.amount}" name="amount"
               aria-label="passport" aria-describedby="basic-addon2"
               value="${book.amount}" required>
    </div>

    <button type="submit"
            class="btn btn-primary btn-lg btn-block">${properties.add_new_book}</button>
</form>
