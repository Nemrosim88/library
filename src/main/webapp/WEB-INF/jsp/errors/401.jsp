<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<html lang="${properties.language}">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="../../img/icons/error-5-multi-size.ico">

    <title>401</title>

    <!-- Bootstrap core CSS -->
    <link href="${context}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${context}/css/error/401.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <%--<link href="signin.css" rel="stylesheet">--%>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse"
            data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="navbar-brand" href="${pageContext.request.contextPath}/index.jsp">
                    <img src="https://png.icons8.com/real-estate/dusk/64" title="Недвижимость" width="30" height="30">
                    ${properties.home}
                </a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <hr>
    <div class="card text-center">
        <div class="card-header">

        </div>
        <div class="card-body">
            <h4 class="card-title">${properties.notAuthorizedTitle}</h4>
            <p class="card-text" style="color: red">${properties.notAuthorizedMessage}</p>
        </div>
        <div class="card-footer text-muted">
            ---
        </div>
    </div>
</div>




</body>
</html>
