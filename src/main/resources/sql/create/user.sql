CREATE TABLE library.user (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(45) NOT NULL,
  surname varchar(45) NOT NULL,
  patronymic varchar(45) DEFAULT NULL,
  dayOfBirth date NOT NULL,
  role enum('admin', 'librarian', 'reader') DEFAULT NULL,
  passport varchar(45) DEFAULT NULL,
  login varchar(45) DEFAULT NULL,
  password varchar(45) DEFAULT NULL,
  email varchar(45) DEFAULT NULL,
  phoneNumber varchar(45) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY id_UNIQUE (id)
);