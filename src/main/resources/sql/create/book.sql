CREATE TABLE library.book (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(300) NOT NULL,
  info varchar(3000) NOT NULL,
  genre varchar(45) NOT NULL,
  authorName varchar(45) NOT NULL,
  authorSurname varchar(45) NOT NULL,
  authorPatronymic varchar(45),
  yearOfEdition date,
  amount int(11),
  issued int(11),
  img_href varchar(45) DEFAULT NULL,
  PRIMARY KEY (id)
)