INSERT INTO library.book (id, name, info, genre,
                          authorName, authorSurname, authorPatronymic,
                          yearOfEdition, amount, issued, img_href)
VALUES
  (1, 'Артемида',
      'Артемида – единственный город на Луне.',
      'Зарубежная фантастика',
      'Энди',
      'Вейер',
      '',
      '2017-01-01',
      '10', '0', ''),

  (2, 'Sapiens. Краткая история человечества',
      'Сто тысяч лет назад Homo sapiens был одним из как минимум шести видов человека.',
      'Научно-популярная литература',
      'Юваль',
      'Ной',
      'Харари',
      '2011-01-01',
      '15', '0', ''),

  (3, 'Гормоны счастья. Как приучить мозг вырабатывать серотонин, дофамин, эндорфин и окситоцин',
      'Автор этой книги предлагает узнать все о работе гормонов.',
      'Научно-популярная литература',
      'Лоретта',
      'Бройнинг',
      '',
      '2016-01-01',
      '12', '0', '')