package control.controller;

import control.commands.CommandFabric;
import data.pool.ConnectionPoolForMariaDB;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/*
By default, a servlet application is located at the path
<Tomcat-installationdirectory>/webapps/ROOT
and the class file would reside in
<Tomcat-installationdirectory>/webapps/ROOT/WEB-INF/classes.
*/

/**
 * Сервлет контроллер
 */
@WebServlet(name = "Controller", urlPatterns = {"/controller",
        "/jsp/controller",
        "/jsp/staff/controller",
        "/jsp/doctor/controller"})
public class Controller extends HttpServlet {

    private final static Logger logger = Logger.getLogger(Controller.class);
    public static Connection connection;
    public static String contextPath;
    private String forward;

    @Override
    public void init() throws ServletException {
        contextPath = getServletContext().getContextPath();
        super.init();
    }

    public static String jspPages(String page) {
        HashMap<String, String> map = new HashMap<>();
        map.put("index", "/index.jsp");
        map.put("start", "/WEB-INF/jsp/start.jsp");
        map.put("addNewUsers", "/WEB-INF/jsp/addNewUsers.jsp");
        map.put("addNewBook", "/WEB-INF/jsp/librarian/addNewBook.jsp");
        map.put("viewAllBooks", "/WEB-INF/jsp/reader/viewAllBooks.jsp");
        map.put("issueBook", "/WEB-INF/jsp/librarian/issueBook.jsp");
        map.put("bookReturn", "/WEB-INF/jsp/librarian/bookReturn.jsp");
        map.put("viewHistory", "/WEB-INF/jsp/librarian/viewHistory.jsp");
        return map.get(page);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        logger.info(": DO GET");

        String pageURLParameter = req.getParameter("page");
        if (pageURLParameter != null && !pageURLParameter.equals("")) {
            getConnectionIfNullOrClosed();
            logger.info(":'pageURLParameter != null && !pageURLParameter.equals(\"\")'");
            RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(jspPages(pageURLParameter));
            try {
                requestDispatcher.forward(req, resp);
            } catch (ServletException | IOException e) {
                e.printStackTrace();
            }
        } else {
            doPost(req, resp);
        }
    }

    /**
     * Сервлет-контроллер для распределения приходящих запросов на исполнителей
     *
     * @param request  request
     * @param response response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        logger.info(": DO_POST");
        logger.info(": connection = " + connection);

        getConnectionIfNullOrClosed();
        logger.info(": connection = " + connection);


        String command = request.getParameter("command");

        forward = new CommandFabric().get(command, connection, request, response);
        logger.info(": forward = " + forward);
        forwardWithRequestDispatcher(request, response);
    }


    private void forwardWithRequestDispatcher(HttpServletRequest req, HttpServletResponse resp) {
        try {
            RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forward);
            requestDispatcher.forward(req, resp);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
    }

    private void getConnectionIfNullOrClosed() {
        logger.info(": getConnectionIfNullOrClosed()");
        try {
            if (connection == null || connection.isClosed()) {
                logger.info(": getting Connection");
                connection = ConnectionPoolForMariaDB.getInstance().getConnection();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }

    }

    public Connection getConnection() {
        return connection;
    }
}
