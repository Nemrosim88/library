package control.commands.utils;

import control.service.SendEmail;
import entity.Book;
import entity.User;
import entity.enums.Role;
import org.apache.commons.lang.RandomStringUtils;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

/**
 * Class contains methods that was most of the time repeated in code.
 */
public class CommandsUtils {

    private CommandsUtils() {
    }

    /**
     * Method for getting user params from request and creating new User Object.
     *
     * @param request request
     * @return User
     * @see Role
     * @see User
     * @see HttpServletRequest
     */
    public static User getUserFromRequest(HttpServletRequest request) {


        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String patronymic = request.getParameter("patronymic");
        Date dayOfBirth = Date.valueOf(request.getParameter("dayOfBirth"));

        Role role = Role.ofString(request.getParameter("role"));
        String passport = request.getParameter("passport");
        String login = request.getParameter("login");
        String password = generateRandomPassword();

        String email = request.getParameter("email");
        String phoneNumber = request.getParameter("phoneNumber");

        User user = new User();

        user.setName(name);
        user.setSurname(surname);
        user.setPatronymic(patronymic);
        user.setDayOfBirth(dayOfBirth);
        user.setRole(role);
        user.setPassport(passport);
        user.setLogin(login);
        user.setPassword(password);
        user.setEmail(email);
        user.setPhoneNumber(phoneNumber);


        return user;
    }



    public static Book getBookFromRequest(HttpServletRequest request) {

        String name = request.getParameter("name");
        String info = request.getParameter("info");
        String genre = request.getParameter("genre");
        String authorName = request.getParameter("authorName");
        String authorSurname = request.getParameter("authorSurname");
        String authorPatronymic = request.getParameter("authorPatronymic");
        Date yearOfEdition = Date.valueOf(request.getParameter("yearOfEdition"));
        int amount = Integer.parseInt(request.getParameter("amount"));

        Book book = new Book();

        book.setName(name);
        book.setInfo(info);
        book.setGenre(genre);
        book.setAuthorName(authorName);
        book.setAuthorSurname(authorSurname);
        book.setAuthorPatronymic(authorPatronymic);
        book.setYearOfEdition(yearOfEdition);
        book.setAmount(amount);

        return book;
    }

    /**
     * Method generating random password with six chars length using letters and numbers.
     *
     * @return generated password
     */
    public static String generateRandomPassword() {
        int length = 6;
        boolean useLetters = true;
        boolean useNumbers = true;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }

    /**
     * Send email to user notifying his of his login (entered by administrator)
     * and random generated password.
     */
    public static void sentEmailToUserWithRegistrationData(User user) {
        try {
            SendEmail.mail(user.getEmail(),
                    user.getLogin(),
                    user.getPassword());
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }
}
