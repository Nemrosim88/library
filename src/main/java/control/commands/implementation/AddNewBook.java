package control.commands.implementation;

import control.commands.abstraction.Command;
import control.commands.utils.CommandsUtils;
import control.controller.Controller;
import data.dao.implementation.EntityDAO;
import data.dao.implementation.utils.BookUtils;
import entity.Book;
import entity.User;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;

public class AddNewBook  implements Command {

    private static final Logger logger = Logger.getLogger(AddNewBook.class);
    private static final String REDIRECT_PAGE = Controller.contextPath
            + "/WEB-INF/jsp/librarian/addNewBook.jsp";

    private static volatile AddNewBook instance;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;

    private EntityDAO<Book> dao;
    private Book bookFromRequest;

    private AddNewBook() {
    }

    public static AddNewBook getInstance(Connection c,
                                         HttpServletRequest req,
                                         HttpServletResponse resp) {
        AddNewBook localInstance = instance;
        if (localInstance == null) {
            synchronized (AddNewBook.class) {
                localInstance = instance;
                connection = c;
                request = req;
                response = resp;

                if (localInstance == null) {
                    instance = localInstance = new AddNewBook();
                }
            }
        }
        return localInstance;
    }

    @Override
    public String execute() {
        logger.info("execute()");
        dao = new EntityDAO<>(connection, BookUtils.getInstance());

        bookFromRequest = CommandsUtils.getBookFromRequest(request);

        if (bookAlreadyExistsInDataBase()) {
            return REDIRECT_PAGE;
        } else {
            inputAndCommitChanges();
            return REDIRECT_PAGE;
        }
    }


    /**
     * Method for checking if user that was from request is already exists in database.
     *
     * @return true - if exists
     */
    private boolean bookAlreadyExistsInDataBase() {
        Book existingBook = dao.isExist(bookFromRequest);
        boolean bookExists = existingBook != null;

        if (bookExists) {
            String loginMessage = "Книга с таким именем, автором и годом издания уже существует";
            request.setAttribute("errorMessage", loginMessage);
            request.setAttribute("book", bookFromRequest);
            return true;
        } else {
            return false;
        }
    }


    /**
     * Private method for inputting new data to database and committing changes.
     *
     * @see User
     */
    private void inputAndCommitChanges() {

        int bookID = dao.insert(bookFromRequest);

        boolean insertionWasSuccessful = (bookID != -1);

        if (insertionWasSuccessful) {
            bookFromRequest.setId(bookID);

            logger.info(":inputAndCommitChanges() ->" +
                    " Insertion Was Successful. Book from request: "
                    + bookFromRequest);
            request.setAttribute("bookID", bookID);
        } else {
            logger.warn(":inputAndCommitChanges() -> " +
                    "Insertion Was NOT Successful. Book from request:" +
                    bookFromRequest);
            request.setAttribute("errorMessage", "ЧТО_ТО НЕ ТАК! ОШИБКА!");

        }

        request.setAttribute("bookID", bookFromRequest.getId());
        request.setAttribute("book", bookFromRequest);
    }
}
