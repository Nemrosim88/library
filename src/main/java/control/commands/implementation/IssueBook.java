package control.commands.implementation;

import control.commands.abstraction.Command;
import control.controller.Controller;
import data.dao.implementation.EntityDAO;
import data.dao.implementation.utils.BookUtils;
import data.dao.implementation.utils.HistoryUtils;
import data.dao.implementation.utils.UserUtils;
import entity.Book;
import entity.History;
import entity.User;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class IssueBook implements Command {

    private static final Logger logger = Logger.getLogger(IssueBook.class);
    private static final String REDIRECT_PAGE = Controller.contextPath
            + "/WEB-INF/jsp/librarian/issueBook.jsp";

    private static volatile IssueBook instance;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;

    private EntityDAO<Book> bookDAO;
    private EntityDAO<User> userDAO;
    private EntityDAO<History> historyDAO;

    private Book bookFromDB;
    private User userFromDb;

    private int bookId;
    private int userId;
    private String info;

    private IssueBook() {
    }

    public static IssueBook getInstance(Connection c,
                                        HttpServletRequest req,
                                        HttpServletResponse resp) {
        connection = c;
        request = req;
        response = resp;
        if (instance == null) {
            instance = new IssueBook();
        }
        return instance;
    }

    @Override
    public String execute() {
        logger.info("execute()");

        getParametersFromRequest();

        if (allValuesAreCorrect()) {
            inputAndCommitChanges();
            return REDIRECT_PAGE;
        } else {
            return REDIRECT_PAGE;
        }
    }

    /**
     * TODO comments
     */
    private void getParametersFromRequest() {
        bookDAO = new EntityDAO<>(connection, BookUtils.getInstance());
        userDAO = new EntityDAO<>(connection, UserUtils.getInstance());
        historyDAO = new EntityDAO<>(connection, HistoryUtils.getInstance());

        bookId = Integer.parseInt(request.getParameter("bookId"));
        userId = Integer.parseInt(request.getParameter("userId"));
        info = request.getParameter("info");
    }


    /**
     * Method for checking IDs from request.
     *
     * @return true - if all values are correct
     */
    private boolean allValuesAreCorrect() {

        List<User> userList = userDAO.getByKey(USER_ID_COLUMN_NAME, userId);
        List<Book> bookList = bookDAO.getByKey(BOOK_ID_COLUMN_NAME, bookId);


        boolean listOfBooksIsNotEmpty = !bookList.isEmpty();
        boolean listOfUsersIsNotEmpty = !userList.isEmpty();

        if (listOfBooksIsNotEmpty && listOfUsersIsNotEmpty) {
            bookFromDB = bookList.get(0);
            userFromDb = userList.get(0);
            return true;

        } else {

            String errorMessage = "Книги или пользователя с таким ID не существует в базе";

            request.setAttribute("info", info);
            request.setAttribute("userId", userId);
            request.setAttribute("bookId", bookId);
            request.setAttribute("message", errorMessage);
            return false;
        }
    }


    /**
     * Private method for inputting new data to database and committing changes.
     */
    private void inputAndCommitChanges() {

        History history = new History();
        history.setBookId(bookId);
        history.setUserId(userId);
        history.setInfo(info);

        // Текущая дата
        Timestamp nowTimeForBookIssue = new Timestamp(new Date().getTime());
        history.setDateOfIssue(nowTimeForBookIssue);


        int historyId = historyDAO.insert(history);

        int issued = bookFromDB.getIssued();
        int amount = bookFromDB.getAmount();

        if (amount == 0) {
            logger.info(":inputAndCommitChanges() ->" +
                    " Insertion AND UPDATE Was NOT Successful. Book's amount is ZERO!");

            String message = "Таких книг на складе больше нет!";
            request.setAttribute("bookMessage", message);

        } else {

            amount--;
            issued++;
            bookFromDB.setIssued(issued);
            bookFromDB.setAmount(amount);

            boolean updateWasSuccessful = bookDAO.update(bookFromDB);

            boolean insertionWasSuccessful = (historyId != -1);

            if (insertionWasSuccessful && updateWasSuccessful) {

                history.setId(historyId);
                logger.info(":inputAndCommitChanges() ->" +
                        " Insertion AND UPDATE Was Successful. History: " + history);

                String message = "Добавление прошло успешно. ID истории:" + historyId
                        + " Количество книг \"" + bookFromDB.getName()
                        + "\" осталось " + bookFromDB.getAmount();
                request.setAttribute("bookMessage", message);

            } else {
                logger.warn(":inputAndCommitChanges() -> " +
                        "Insertion AND UPDATE Was NOT Successful. History :" + history);

                String errorMessage = "Добавление в базу прошло с ошибкой. Обратитесь к администратору";

                request.setAttribute("info", info);
                request.setAttribute("userId", userId);
                request.setAttribute("bookId", bookId);
                request.setAttribute("bookMessage", errorMessage);
            }
        }
    }
}
