package control.commands.implementation;

import control.commands.abstraction.Command;
import control.commands.utils.CommandsUtils;
import control.controller.Controller;
import data.dao.implementation.EntityDAO;
import data.dao.implementation.utils.UserUtils;
import entity.User;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;


public class AddNewUser implements Command {

    private static final Logger logger = Logger.getLogger(AddNewUser.class);
    private static final String REDIRECT_PAGE = Controller.contextPath
            + "/WEB-INF/jsp/addNewUsers.jsp";

    private static volatile AddNewUser instance;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;

    private EntityDAO<User> dao;
    private User userFromRequest;

    private AddNewUser() {
    }

    public static AddNewUser getInstance(Connection c,
                                         HttpServletRequest req,
                                         HttpServletResponse resp) {
        AddNewUser localInstance = instance;
        if (localInstance == null) {
            synchronized (AddNewUser.class) {
                localInstance = instance;
                connection = c;
                request = req;
                response = resp;

                if (localInstance == null) {
                    instance = localInstance = new AddNewUser();
                }
            }
        }
        return localInstance;
    }

    @Override
    public String execute() {
        logger.info("execute()");
        dao = new EntityDAO<>(connection, UserUtils.getInstance());

        userFromRequest = CommandsUtils.getUserFromRequest(request);

        if (userAlreadyExistsInDataBase()) {
            return REDIRECT_PAGE;
        } else {
            inputAndCommitChanges();
            return REDIRECT_PAGE;
        }
    }


    /**
     * Method for checking if user that was from request is already exists in database.
     *
     * @return true - if exists
     */
    private boolean userAlreadyExistsInDataBase() {
        User existingUser = dao.isExist(userFromRequest);
        boolean userExists = existingUser != null;

        if (userExists) {
            String loginMessage = "Пользователь с таким паспортом или логином уже существует";
            request.setAttribute("errorMessage", loginMessage);
            request.setAttribute("user", userFromRequest);
            return true;
        } else {
            return false;
        }
    }


    /**
     * Private method for inputting new data to database and committing changes.
     *
     * @see User
     */
    private void inputAndCommitChanges() {

        int userId = dao.insert(userFromRequest);

        boolean insertionWasSuccessful = (userId != -1);

        if (insertionWasSuccessful) {
            userFromRequest.setId(userId);

            logger.info(":inputAndCommitChanges() ->" +
                    " Insertion Was Successful. User from request: "
                    + userFromRequest);
            CommandsUtils.sentEmailToUserWithRegistrationData(userFromRequest);
        } else {
            logger.warn(":inputAndCommitChanges() -> " +
                    "Insertion Was NOT Successful. User from request:" +
                    userFromRequest);
        }

        request.setAttribute("userID", userFromRequest.getId());
        request.setAttribute("user", userFromRequest);
    }


}





