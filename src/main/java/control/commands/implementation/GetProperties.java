package control.commands.implementation;

import control.commands.abstraction.Command;
import control.controller.Controller;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetProperties implements Command {

    private final static Logger logger = Logger.getLogger(GetProperties.class);
    private static GetProperties command;

    private static HttpServletResponse response;
    private static HttpServletRequest request;
    private static Connection connection;
    private String jspPage;


    private GetProperties() {
    }

    public static GetProperties getInstance(Connection conn,
                                            HttpServletRequest req,
                                            HttpServletResponse resp) {
        request = req;
        response = resp;
        connection = conn;
        if (command == null) {
            command = new GetProperties();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info(": execute()");

        String language = request.getParameter("language");

        switch (language) {
            case "ru":
                return setProperties(Controller.contextPath + "/i18n/text_ru.properties");

            case "uk":
                return setProperties(Controller.contextPath + "/i18n/text_uk.properties");

            case "us":
                return setProperties(Controller.contextPath + "/i18n/text.properties");

            case "fr":
                return setProperties(Controller.contextPath + "/i18n/text_fr.properties");

            default:
                return setProperties(Controller.contextPath + "/i18n/text.properties");
        }
    }

    private String setProperties(String filePathAndName) {
        logger.info(": setProperties()");

        Properties properties = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream is = loader.getResourceAsStream(filePathAndName);

        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(is, "UTF-8");
            properties.load(inputStreamReader);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
                if (inputStreamReader != null) {
                    inputStreamReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        getJspPageName();

        request.getSession().setAttribute("properties", properties);
        return Controller.contextPath + jspPage;
    }


    private void getJspPageName() {
        logger.info(": getJspPageName()");

        String pageParam = request.getParameter("jspPage");
        if (pageParam != null) {

            switch (pageParam) {
                case "addNewUsers":
                    jspPage = "/WEB-INF/jsp/addNewUsers.jsp";
                    break;
                case "addNewBook":
                    jspPage = "/WEB-INF/jsp/addNewBook.jsp";
                    break;
                case "issueBook":
                    jspPage = "/WEB-INF/jsp/issueBook.jsp";
                    break;
                case "index":
                    jspPage = "/index.jsp";
                    break;
                case "staffTasks":
                    jspPage = "/jsp/staff/tasks.jsp";
                    break;
                default:
                    jspPage = "/";
            }
        }
    }

}


