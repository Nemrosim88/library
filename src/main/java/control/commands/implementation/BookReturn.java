package control.commands.implementation;

import control.commands.abstraction.Command;
import control.controller.Controller;
import data.dao.implementation.EntityDAO;
import data.dao.implementation.utils.BookUtils;
import data.dao.implementation.utils.HistoryUtils;
import entity.Book;
import entity.History;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class BookReturn implements Command {

    private static final Logger logger = Logger.getLogger(BookReturn.class);
    private static final String REDIRECT_PAGE = Controller.contextPath
            + "/WEB-INF/jsp/librarian/bookReturn.jsp";
    private static final String MESSAGE_NAME = "bookMessage";

    private static BookReturn instance;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;

    private EntityDAO<History> historyDAO;
    private EntityDAO<Book> bookDAO;

    private History historyFromDB;

    private int historyId;

    private BookReturn() {
    }

    public static BookReturn getInstance(Connection c,
                                         HttpServletRequest req,
                                         HttpServletResponse resp) {
        connection = c;
        request = req;
        response = resp;
        if (instance == null) {
            instance = new BookReturn();
        }
        return instance;
    }

    @Override
    public String execute() {
        logger.info("execute()");

        if (getParametersFromRequestWasSuccessful() && allValuesAreCorrect()) {
            inputAndCommitChanges();
            return REDIRECT_PAGE;
        } else {
            return REDIRECT_PAGE;
        }
    }

    /**
     * TODO comments
     */
    private boolean getParametersFromRequestWasSuccessful() {
        historyId = Integer.parseInt(request.getParameter("historyId"));
        if (historyId >= 0) {
            historyDAO = new EntityDAO<>(connection, HistoryUtils.getInstance());
            return true;
        } else {
            String errorMessage = "ID не может равняться  или быть меньше нуля";
            request.setAttribute(MESSAGE_NAME, errorMessage);
            return false;
        }
    }


    /**
     * Method for checking IDs from request.
     *
     * @return true - if all values are correct
     */
    private boolean allValuesAreCorrect() {
        List<History> historyList = historyDAO.getByKey(HISTORY_ID_COLUMN_NAME, historyId);

        boolean listOfHistoryIsNotEmpty = !historyList.isEmpty();

        if (listOfHistoryIsNotEmpty) {
            historyFromDB = historyList.get(0);

            if (historyFromDB.getReturnDate() == null) {
                return true;
            } else {
                String errorMessage = "Книга уже была возвращена. Проверьте данные в БД";
                request.setAttribute(MESSAGE_NAME, errorMessage);
                return false;
            }

        } else {
            String errorMessage = "Истории с таким ID не существует";
            request.setAttribute(MESSAGE_NAME, errorMessage);
            return false;
        }
    }


    /**
     * Private method for inputting new data to database and committing changes.
     */
    private void inputAndCommitChanges() {

        // Текущая дата возврата книги
        Timestamp nowTimeForBookIssue = new Timestamp(new Date().getTime());
        historyFromDB.setReturnDate(nowTimeForBookIssue);

        boolean updateSuccessful = historyDAO.update(historyFromDB);

        if (updateSuccessful) {
            bookDAO = new EntityDAO<>(connection, BookUtils.getInstance());

            Book book = bookDAO.getByKey(BOOK_ID_COLUMN_NAME, historyFromDB.getBookId()).get(0);

            int amountOfBooks = book.getAmount();
            int bookIssued = book.getIssued();
            amountOfBooks++;
            bookIssued--;
            book.setIssued(bookIssued);
            book.setAmount(amountOfBooks);

            boolean updateWasSuccessful = bookDAO.update(book);

            if (updateWasSuccessful) {
                logger.info(":inputAndCommitChanges() ->" +
                        " Insertion AND UPDATE Was Successful. Book: " + book);

                String message = "Добавление прошло успешно. ID истории:" + historyId
                        + " Количество книг \"" + book.getName()
                        + "\" осталось " + book.getAmount();
                request.setAttribute(MESSAGE_NAME, message);

            } else {
                logger.warn(":inputAndCommitChanges() -> " +
                        "Insertion AND UPDATE Was NOT Successful. Book :" + book);

                String errorMessage = "Добавление в базу прошло с ошибкой. Обратитесь к администратору";
                request.setAttribute(MESSAGE_NAME, errorMessage);
            }
        }
    }
}

