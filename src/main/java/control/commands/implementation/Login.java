package control.commands.implementation;

import control.commands.abstraction.Command;
import control.controller.Controller;
import control.service.Cookies;


import data.dao.implementation.EntityDAO;
import data.dao.implementation.utils.UserUtils;
import entity.*;
import entity.enums.Role;
import org.apache.log4j.Logger;

import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Login implements Command {

    private static final Logger logger = Logger.getLogger(Login.class);
    private static final String REDIRECT_PAGE = Controller.contextPath + "/";

    private static Login command;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;
    private static User userEntity;

    private EntityDAO<User> dao;
    private Role userRole;
    private String login;


    private static final String LOGIN_PARAM = "login";
    private static final String MESSAGE_PARAM = "message";
    private static final String PASSWORD_PARAM = "password";
    private static final String ERROR_MESSAGE = "Nickname or Password doesn't match ";

    private Login() {
    }

    public static Login getInstance(Connection conn,
                                    HttpServletRequest req,
                                    HttpServletResponse resp) {
        connection = conn;
        request = req;
        response = resp;
        if (command == null) {
            command = new Login();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info(": execute()");
        logger.info(": connection = " + connection);

        dao = new EntityDAO<>(connection, UserUtils.getInstance());

        if (userExistsInDB()) {
            setValuesForResponse();
            new Cookies(response, userEntity);
        } else {
            request.setAttribute(LOGIN_PARAM, login);
            request.setAttribute(MESSAGE_PARAM, ERROR_MESSAGE);
        }

        return REDIRECT_PAGE;
    }


    /**
     * Private method for setting values for response to user back to browser.
     */
    private void setValuesForResponse() {
        logger.info(": setValuesForResponse()");

        checkRoleOfUserAndSetAppropriateAttributesToSession();

        request.getSession().setAttribute("user", userEntity);
        request.getSession().setAttribute("logged", userRole);
    }


    /**
     * Private method for checking user's role and depending of -
     * setting appropriate attributes to session.
     */
    private void checkRoleOfUserAndSetAppropriateAttributesToSession() {
        int userId = userEntity.getId();
        userRole = userEntity.getRole();
        logger.info(": USER LOGGED IN. Role == " + userRole + " with ID:" + userId);

        String textForLogger = ": USER THAT LOGGED IN is: ";

        logger.info(textForLogger + userEntity.getRole().toString());

    }


    /**
     * Private method for checking existence of login AND password in database.
     *
     * @return if login and password exists in database - true
     */
    private boolean userExistsInDB() {
        logger.info(": userExistsInDB()");

        login = request.getParameter(LOGIN_PARAM);
        String password = request.getParameter(PASSWORD_PARAM);

        User userFromRequest = new User(login, password);

        userEntity = dao.isExist(userFromRequest);
        return userEntity != null;
    }

}