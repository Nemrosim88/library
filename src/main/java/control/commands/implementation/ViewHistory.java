package control.commands.implementation;

import control.commands.abstraction.Command;
import control.controller.Controller;
import data.dao.implementation.EntityDAO;
import data.dao.implementation.utils.BookUtils;
import data.dao.implementation.utils.HistoryUtils;
import data.dao.implementation.utils.UserUtils;
import entity.Book;
import entity.History;
import entity.User;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.util.List;

public class ViewHistory implements Command {

    private static final Logger logger = Logger.getLogger(ViewHistory.class);
    private static final String REDIRECT_PAGE = Controller.contextPath
            + Controller.jspPages("viewHistory");

    private static volatile ViewHistory instance;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;

    private EntityDAO<Book> bookDAO;
    private EntityDAO<User> userDAO;
    private EntityDAO<History> historyDAO;



    private int bookId;
    private int userId;
    private String info;

    private ViewHistory() {
    }

    public static ViewHistory getInstance(Connection c,
                                        HttpServletRequest req,
                                        HttpServletResponse resp) {
        connection = c;
        request = req;
        response = resp;
        if (instance == null) {
            instance = new ViewHistory();
        }
        return instance;
    }

    @Override
    public String execute() {
        logger.info("execute()");

        getAllHistoryValues();


            return REDIRECT_PAGE;



    }

    /**
     * TODO comments
     */
    private void getAllHistoryValues() {
        historyDAO = new EntityDAO<>(connection, HistoryUtils.getInstance());

        bookDAO = new EntityDAO<>(connection, BookUtils.getInstance());
        userDAO = new EntityDAO<>(connection, UserUtils.getInstance());

        Book bookFromDB;
        User userFromDb;

        List<History> historyList = historyDAO.getAll();

        for (History history : historyList) {

           int bookId =  history.getBookId();
           int userId = history.getUserId();

           bookFromDB = bookDAO.getByKey(BOOK_ID_COLUMN_NAME, bookId).get(0);
           userFromDb = userDAO.getByKey(USER_ID_COLUMN_NAME, userId).get(0);

           history.setUser(userFromDb);
           history.setBook(bookFromDB);

        }

        request.setAttribute("historyList", historyList);

    }


}
