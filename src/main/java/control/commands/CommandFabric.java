package control.commands;

import control.commands.implementation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;

import org.apache.log4j.Logger;

/**
 * This class is implementing fabric pattern. Depending on command from html form.
 */
public class CommandFabric {

    private final static Logger logger = Logger.getLogger(CommandFabric.class);

    /**
     * Метод для получения нужного экземпляра класса комманды.
     *
     * @param command    текстовое представление команды. "login" - создаётся Login
     * @param connection connection
     * @param request    request
     * @param response   response
     * @return возвращает строку - результат работы команды
     */
    public String get(String command,
                      Connection connection,
                      HttpServletRequest request,
                      HttpServletResponse response) {
        logger.info(String.format(": get() method. Variables: " +
                "command = %s, " +
                "connection = %s, " +
                "request = %s, " +
                "response = "
                + "%s", command, connection, request, response));

        switch (command) {
            case "login":
                return Login.getInstance(connection, request, response).execute();
            case "addNewUser":
                return AddNewUser.getInstance(connection, request, response).execute();
            case "addNewBook":
                return AddNewBook.getInstance(connection, request, response).execute();
            case "issueBook":
                return IssueBook.getInstance(connection, request, response).execute();
            case "bookReturn":
                return BookReturn.getInstance(connection, request, response).execute();
            case "getProperties":
                return GetProperties.getInstance(connection, request, response).execute();
            case "viewHistory":
                return ViewHistory.getInstance(connection, request, response).execute();
            case "logout":
                return InvalidateSession.getInstance(connection, request, response).execute();
            default:
                logger.warn("Unknown COMMAND");
                return "/";
        }

    }


}
