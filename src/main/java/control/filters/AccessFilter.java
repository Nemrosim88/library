package control.filters;


import entity.enums.Role;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import org.apache.log4j.Logger;

public class AccessFilter implements Filter {

    private final static Logger logger = Logger.getLogger(AccessFilter.class);
    private static HttpServletRequest httpRequest;
    private static HttpServletResponse httpResponse;
    private static String requestURI;
    private static Role role;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info(": init()");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {
        logger.info(": doFilter()");

        httpRequest = (HttpServletRequest) request;
        httpResponse = (HttpServletResponse) response;

        role = (Role) httpRequest.getSession().getAttribute("logged");

        requestURI = httpRequest.getRequestURI();

        if (role == null) {
            //  logAccessDeniedWithUserParams();
            checking(chain);
        } else {
            checking(chain);
        }
    }

    @Override
    public void destroy() {
        logger.info(": destroy()");
    }

    private void checking(FilterChain chain) {

        System.out.println("requestURI ----------->>>>>>" + requestURI);

        if (role == Role.ADMIN) {
            chain(chain);
        } else if (role == Role.LIBRARIAN && (requestURI.startsWith("/WEB-INF/jsp/librarian/")
                || requestURI.startsWith("/WEB-INF/jsp/reader/"))) {
            chain(chain);
        } else if (role == Role.READER && requestURI.startsWith("/WEB-INF/jsp/reader/")) {
            chain(chain);
        } else {

// TODO переместить всё в нужные папки
            switch (requestURI) {

                // ----------- Pages available for DOCTORS ----------------
                case "/jsp/admin.jsp":
                    if (role != Role.ADMIN) {
                        logAccessDeniedWithUserParams();
                    } else {
                        chain(chain);
                    }
                    break;

                case "/jsp/addPatient.jsp":
                    if (role != Role.ADMIN) {
                        logAccessDeniedWithUserParams();
                    } else {
                        chain(chain);
                    }
                    break;
                case "/jsp/addDoctor.jsp":
                    if (role != Role.ADMIN) {
                        logAccessDeniedWithUserParams();
                    } else {
                        chain(chain);
                    }
                    break;
                case "/jsp/addStaff.jsp":
                    if (role != Role.ADMIN) {
                        logAccessDeniedWithUserParams();
                    } else {
                        chain(chain);
                    }
                    break;

                case "/jsp/addAdmin.jsp":
                    if (role != Role.ADMIN) {
                        logAccessDeniedWithUserParams();
                    } else {
                        chain(chain);
                    }
                    break;

                // ----------- Pages available for PATIENTS ----------------
                case "/jsp/patient.jsp":
                    if (role != Role.ADMIN) {
                        logAccessDeniedWithUserParams();
                    } else {
                        chain(chain);
                    }
                    break;


                case "/index.jsp":
                    chain(chain);
                    break;

                case "/jsp/controller":
                    chain(chain);
                    break;

                default:
                    logAccessDeniedWithUserParams();
                    break;
            }
        }

    }

    private void logAccessDeniedWithUserParams() {
        String logString = String.format("ACCESS DENIED!! "
                        + "\n User params:"
                        + "\n 1.RequestURI:'%s';"
                        + "\n 2.AuthType:'%s';"
                        + "\n 3.ContextPath:'%s';"
                        + "\n 4.Method:'%s';"
                        + "\n 5.QueryString:'%s';"
                        + "\n 6.RemoteUser:'%s';"
                        + "\n 7.ServletPath:'%s';"
                        + "\n 8.PathInfo:'%s';"
                        + "\n 9.PathTranslated:'%s';"
                        + "\n 10.RemoteHost:'%s';"
                        + "\n 11.RemotePort:'%s';"
                        + "\n 12.ServerName:'%s';\n",
                httpRequest.getRequestURI(),
                httpRequest.getAuthType(),
                httpRequest.getContextPath(),
                httpRequest.getMethod(),
                httpRequest.getQueryString(),
                httpRequest.getRemoteUser(),
                httpRequest.getServletPath(),
                httpRequest.getPathInfo(),
                httpRequest.getPathTranslated(),
                httpRequest.getRemoteHost(),
                httpRequest.getRemotePort(),
                httpRequest.getServerName());
        logger.warn("Trying to access: '" + requestURI + "' \n " + logString);
        try {
            httpResponse.sendError(401);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
    }

    private void chain(FilterChain chain) {
        try {
            logger.info(httpRequest);
            logger.info(httpResponse);
            chain.doFilter(httpRequest, httpResponse);
        } catch (IOException | ServletException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
    }
}
