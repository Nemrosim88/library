package control.service;

import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class SendEmail {

    private final static Logger logger = Logger.getLogger(SendEmail.class);
    private final static String EMAIL_ADDRESS = "demo_library@ukr.net" ;

    private SendEmail() {
    }

    /**
     * Метод используется для отправки почты с логином и паролем.
     *
     * @param email       кому
     * @param login       логин пользователя
     * @param password    пароль пользователя
     * @throws MessagingException MessagingException
     */
    public static void mail(String email, String login, String password) throws MessagingException {

        logger.info(": mail method in SendEmail class");

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.ukr.net");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        Session session = Session.getDefaultInstance(props,
                new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(EMAIL_ADDRESS,
                                "librarY_DEMO!234");
                    }
                });

        try {
            String msg = "<head>\n" +
                    "Your registration data to <b>demo_library.nemrosim.com.ua</b>" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div>\n" +
                    
                    "<h4>Your login is:</h1>\n" +
                    "<h3>" + login + "</h1>\n" +
                    "<h4>Your password is:</h1>\n" +
                    "<h2>" + password + "</h1>\n" +

                    "</div>\n" +
                    "</body>\n" +
                    "</html>";
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(EMAIL_ADDRESS));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email));
            message.setContent(msg, "text/html; charset=utf-8");
            message.setSubject("Your have been registered to <b>demo_library.nemrosim.com.ua</b>");
//            message.setText(msg);

            Transport.send(message);

            logger.info("Email sent");

        } catch (MessagingException e) {
            logger.info("Email wasn't sent");
            throw new RuntimeException(e);
        }

    }
}
