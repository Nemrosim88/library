package data;

/**
 * Constants for schema "library".
 */
public interface Constants {

    public static final String SCHEMA_NAME = "library";

    // Names for "user" table in "library" schema
    String USER_TABLE_NAME = "user";
    String USER_ID_COLUMN_NAME = "id";
    String USER_NAME_COLUMN_NAME = "name";
    String USER_SURNAME_COLUMN_NAME = "surname";
    String USER_PATRONYMIC_COLUMN_NAME = "patronymic";
    String USER_DAY_OF_BIRTH_COLUMN_NAME = "dayOfBirth";
    String USER_ROLE_COLUMN_NAME = "role";
    String USER_PASSPORT_COLUMN_NAME = "passport";
    String USER_LOGIN_COLUMN_NAME = "login";
    String USER_PASSWORD_COLUMN_NAME = "password";
    String USER_EMAIL_COLUMN_NAME = "email";
    String USER_PHONE_NUMBER_COLUMN_NAME = "phoneNumber";

    // Names for "book" table in "library" schema
    String BOOK_TABLE_NAME = "book";
    String BOOK_ID_COLUMN_NAME = "id";
    String BOOK_NAME_COLUMN_NAME = "name";
    String BOOK_INFO_COLUMN_NAME = "info";
    String BOOK_GENRE_COLUMN_NAME = "genre";
    String BOOK_AUTHOR_NAME_COLUMN_NAME = "authorName";
    String BOOK_AUTHOR_SURNAME_COLUMN_NAME = "authorSurname";
    String BOOK_AUTHOR_PATRONYMIC_COLUMN_NAME = "authorPatronymic";
    String BOOK_YEAR_OF_EDITION_COLUMN_NAME = "yearOfEdition";
    String BOOK_AMOUNT_COLUMN_NAME = "amount";
    String BOOK_ISSUED_COLUMN_NAME = "issued";
    String BOOK_IMG_HREF_COLUMN_NAME = "img_href";


    // Names for "history" table in "library" schema
    String HISTORY_TABLE_NAME = "history";
    String HISTORY_ID_COLUMN_NAME = "id";
    String HISTORY_BOOK_ID_COLUMN_NAME = "bookId";
    String HISTORY_USER_ID_COLUMN_NAME = "userId";
    String HISTORY_DATE_OF_ISSUE_COLUMN_NAME = "dateOfIssue";
    String HISTORY_RETURN_DATE_COLUMN_NAME = "returnDate";
    String HISTORY_INFO_COLUMN_NAME = "info";
}
