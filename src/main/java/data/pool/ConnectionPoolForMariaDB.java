package data.pool;

import data.pool.abstraction.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Thread-safe singleton.
 */
public class ConnectionPoolForMariaDB implements ConnectionPool {

    private final static Logger logger = Logger.getLogger(ConnectionPoolForMariaDB.class);
    private static volatile ConnectionPoolForMariaDB instance = null;
    private static DataSource dataSource = null;

    private ConnectionPoolForMariaDB() {
        logger.info(": ConnectionPoolForMariaDB()");
        try {
            InitialContext ic = new InitialContext();
            dataSource = (DataSource) ic.lookup("java:/comp/env/jdbc/mariadb");
        } catch (NamingException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
    }

    public static ConnectionPoolForMariaDB getInstance() {
        ConnectionPoolForMariaDB localInstance = instance;
        if (localInstance == null) {
            synchronized (ConnectionPoolForMariaDB.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ConnectionPoolForMariaDB();
                }
            }
        }
        return localInstance;
    }

    @Override
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public void freeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
    }
}
