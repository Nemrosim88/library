package data.dao.abstraction;

import entity.abstraction.Entity;

import java.util.List;

public interface DAO<E extends Entity> {

    List<E> getAll() throws Exception;

    List<E> getByKey(String keyName, int key) throws Exception;

    List<E> getByValue(String valueName, String value) throws Exception;

    int insert(E entity) throws Exception;

    boolean update(E entity) throws Exception;

    boolean delete(int id) throws Exception;

    E isExist(E entity) throws Exception;

}
