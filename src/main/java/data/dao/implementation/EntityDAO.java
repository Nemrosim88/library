package data.dao.implementation;

import data.dao.abstraction.AbstractClosing;
import data.dao.abstraction.DAO;
import data.dao.abstraction.EntityUtils;
import entity.abstraction.Entity;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class EntityDAO<E extends Entity> extends AbstractClosing
        implements DAO<E> {

  private Connection connection;
  private EntityUtils<E> utils;
  private final static Logger logger = Logger.getLogger(EntityDAO.class);

  public EntityDAO(Connection connection, EntityUtils<E> utils) {
    this.connection = connection;
    this.utils = utils;
  }

  @Override
  public List<E> getAll() {
    Statement statement = null;
    ResultSet resultSet = null;
    try {
      statement = connection.createStatement();
      resultSet = statement.executeQuery(utils.getAll());
      List<E> list = new ArrayList<>();
      while (resultSet.next()) {
        list.add(utils.getFromResultSet(resultSet));
      }
      return list;
    } catch (SQLException e) {
      e.printStackTrace();
      logger.error(e.getLocalizedMessage());
      return null;
    } finally {
      closeStatementAndResultSet(statement, resultSet);
    }
  }

  @Override
  public List<E> getByKey(String keyName, int key) {
    PreparedStatement statement = null;
    ResultSet resultSet = null;
    try {
      statement = utils.getByKey(connection, keyName, key);
      resultSet = statement.executeQuery();
      List<E> list = new ArrayList<>();
      while (resultSet.next()) {
        list.add(utils.getFromResultSet(resultSet));
      }
      return list;
    } catch (SQLException e) {
      e.printStackTrace();
      logger.error(e.getLocalizedMessage());
      return null;
    } finally {
      closeStatementAndResultSet(statement, resultSet);
    }
  }

  @Override
  public List<E> getByValue(String valueName, String value) {
    PreparedStatement statement = null;
    ResultSet resultSet = null;
    try {
      statement = utils.getByValue(connection, valueName, value);
      resultSet = statement.executeQuery();
      List<E> list = new ArrayList<>();
      while (resultSet.next()) {
        list.add(utils.getFromResultSet(resultSet));
      }
      return list;
    } catch (SQLException e) {
      e.printStackTrace();
      logger.error(e.getLocalizedMessage());
      return null;
    } finally {
      closeStatementAndResultSet(statement, resultSet);
    }
  }

  @Override
  public int insert(E entity) {
    PreparedStatement statement = null;
    ResultSet resultSet = null;
    int generatedId;
    try {
      statement = utils.insert(connection, entity);
      statement.executeUpdate();
      resultSet = statement.getGeneratedKeys();
      resultSet.next();
      generatedId = resultSet.getInt(1);
      return generatedId;
    } catch (SQLException e) {
      e.printStackTrace();
      logger.error(e.getLocalizedMessage());
      return -1;
    } finally {
      closeStatementAndResultSet(statement, resultSet);
    }
  }

  @Override
  public boolean update(E entity) {
    PreparedStatement statement = null;
    try {
      statement = utils.update(connection, entity);
      statement.executeUpdate();
      return true;
    } catch (SQLException e) {
      e.getMessage();
      logger.error(e.getLocalizedMessage());
      return false;
    } finally {
      closeStatement(statement);
    }
  }

  @Override
  public boolean delete(int id) {
    PreparedStatement statement = null;
    try {
      statement = utils.delete(connection, id);
      statement.executeUpdate();
      return true;
    } catch (SQLException e) {
      e.printStackTrace();
      logger.error(e.getLocalizedMessage());
      return false;
    } finally {
      closeStatement(statement);
    }
  }

  @Override
  public E isExist(E entity) {
    PreparedStatement statement = null;
    ResultSet resultSet = null;
    try {
      statement = utils.isExist(connection, entity);
      resultSet = statement.executeQuery();
      if (resultSet.next()) {
        return utils.getFromResultSet(resultSet);
      } else {
        return null;
      }
    } catch (SQLException e) {
      e.printStackTrace();
      logger.error(e.getLocalizedMessage());
      return null;
    } finally {
      closeStatementAndResultSet(statement, resultSet);
    }
  }
}
