package data.dao.implementation.utils;

import data.dao.abstraction.EntityUtils;
import entity.Book;

import java.sql.*;

/**
 * Thread-safe singleton for class that is used in DAO for "book".
 */
public class BookUtils implements EntityUtils<Book> {

    private static volatile BookUtils instance;

    private BookUtils() {
    }

    public static BookUtils getInstance() {
        BookUtils localInstance = instance;
        if (localInstance == null) {
            synchronized (BookUtils.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new BookUtils();
                }
            }
        }
        return localInstance;
    }

    @Override
    public String getAll() {
        return String.format("SELECT * FROM %s.%s", SCHEMA_NAME, BOOK_TABLE_NAME);
    }

    @Override
    public PreparedStatement getByKey(Connection connection,
                                      String keyName,
                                      int key) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s=?",
                SCHEMA_NAME,
                BOOK_TABLE_NAME,
                keyName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, key);
        return statement;
    }

    @Override
    public PreparedStatement getByValue(Connection connection,
                                        String valueName,
                                        String value) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s=?",
                SCHEMA_NAME,
                BOOK_TABLE_NAME,
                valueName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, value);
        return statement;
    }

    @Override
    public PreparedStatement insert(Connection connection, Book entity) throws SQLException {
        String query = String.format("INSERT INTO %s.%s " +
                               "(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) " +
                        "VALUES (?,  ?,  ?,  ?,  ?,  ?,  ?,  ?,  ?,  ?)",
                // INSERT ----------------
                SCHEMA_NAME,
                BOOK_TABLE_NAME,
                // COLUMN NAMES ----------
                BOOK_NAME_COLUMN_NAME,
                BOOK_INFO_COLUMN_NAME,
                BOOK_GENRE_COLUMN_NAME,
                BOOK_AUTHOR_NAME_COLUMN_NAME,
                BOOK_AUTHOR_SURNAME_COLUMN_NAME,
                BOOK_AUTHOR_PATRONYMIC_COLUMN_NAME,
                BOOK_YEAR_OF_EDITION_COLUMN_NAME,
                BOOK_AMOUNT_COLUMN_NAME,
                BOOK_ISSUED_COLUMN_NAME,
                BOOK_IMG_HREF_COLUMN_NAME);
        PreparedStatement statement =
                connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, entity.getName());
        statement.setString(2, entity.getInfo());
        statement.setString(3, entity.getGenre());
        statement.setString(4, entity.getAuthorName());
        statement.setString(5, entity.getAuthorSurname());
        statement.setString(6, entity.getAuthorPatronymic());
        statement.setDate(7, entity.getYearOfEdition());
        statement.setInt(8, entity.getAmount());
        statement.setInt(9, entity.getIssued());
        statement.setString(10, entity.getImg_href());
        return statement;
    }

    @Override
    public PreparedStatement update(Connection connection, Book entity) throws SQLException {
        String query =
                String.format("UPDATE %s.%s SET " +
                                //Name  Info  Genre
                                "%s= ?, %s= ?, %s= ?, " +
                                //A_Name A_Surname A_Patronymic
                                "%s= ?, %s= ? , %s= ?, " +
                                //YearOfEdition   Amount
                                "%s= ?, %s= ?, " +
                                //Issued   Img_href
                                "%s= ?, %s= ? " +
                                "WHERE id = ?;",
                        //UPDATE -----------------
                        SCHEMA_NAME,
                        BOOK_TABLE_NAME,

                        //SET --------------------
                        BOOK_NAME_COLUMN_NAME,
                        BOOK_INFO_COLUMN_NAME,
                        BOOK_GENRE_COLUMN_NAME,

                        BOOK_AUTHOR_NAME_COLUMN_NAME,
                        BOOK_AUTHOR_SURNAME_COLUMN_NAME,
                        BOOK_AUTHOR_PATRONYMIC_COLUMN_NAME,

                        BOOK_YEAR_OF_EDITION_COLUMN_NAME,
                        BOOK_AMOUNT_COLUMN_NAME,

                        BOOK_ISSUED_COLUMN_NAME,
                        BOOK_IMG_HREF_COLUMN_NAME);

        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getName());
        statement.setString(2, entity.getInfo());
        statement.setString(3, entity.getGenre());

        statement.setString(4, entity.getAuthorName());
        statement.setString(5, entity.getAuthorSurname());
        statement.setString(6, entity.getAuthorPatronymic());

        statement.setDate(7, entity.getYearOfEdition());
        statement.setInt(8, entity.getAmount());

        statement.setInt(9, entity.getIssued());
        statement.setString(10, entity.getImg_href());

        statement.setInt(11, entity.getId());
        return statement;

    }

    @Override
    public PreparedStatement delete(Connection connection, int id) throws SQLException {
        String query = String.format("DELETE FROM %s.%s WHERE %s = ?;",
                // DELETE  -----------
                SCHEMA_NAME,
                BOOK_TABLE_NAME,
                // WHERE -------------
                BOOK_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        return statement;
    }

    /**
     * TODO перевод на англ
     * Проверка книги проводиться по назнанию и году издания
     *
     * @param connection
     * @param entity     entity
     * @return
     * @throws SQLException
     */
    @Override
    public PreparedStatement isExist(Connection connection, Book entity) throws SQLException {
        String query =
                String.format("SELECT * FROM %s.%s WHERE %s=? AND %s=? AND %s=?;",
                        // SELECT --------------
                        SCHEMA_NAME,
                        BOOK_TABLE_NAME,
                        // WHERE ---------------
                        BOOK_NAME_COLUMN_NAME,
                        BOOK_YEAR_OF_EDITION_COLUMN_NAME,
                        BOOK_AUTHOR_SURNAME_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getName());
        statement.setDate(2, entity.getYearOfEdition());
        statement.setString(3, entity.getAuthorSurname());
        return statement;
    }

    @Override
    public Book getFromResultSet(ResultSet resultSet) throws SQLException {
        return new Book(
                resultSet.getInt(BOOK_ID_COLUMN_NAME),
                resultSet.getString(BOOK_NAME_COLUMN_NAME),
                resultSet.getString(BOOK_INFO_COLUMN_NAME),
                resultSet.getString(BOOK_GENRE_COLUMN_NAME),

                resultSet.getString(BOOK_AUTHOR_NAME_COLUMN_NAME),
                resultSet.getString(BOOK_AUTHOR_SURNAME_COLUMN_NAME),
                resultSet.getString(BOOK_AUTHOR_PATRONYMIC_COLUMN_NAME),

                resultSet.getDate(BOOK_YEAR_OF_EDITION_COLUMN_NAME),

                resultSet.getInt(BOOK_AMOUNT_COLUMN_NAME),
                resultSet.getInt(BOOK_ISSUED_COLUMN_NAME),
                resultSet.getString(BOOK_IMG_HREF_COLUMN_NAME));
    }
}
