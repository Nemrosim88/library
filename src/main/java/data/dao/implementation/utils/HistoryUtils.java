package data.dao.implementation.utils;

import data.dao.abstraction.EntityUtils;
import entity.History;

import java.sql.*;

public class HistoryUtils implements EntityUtils<History> {

    private static volatile HistoryUtils instance;

    private HistoryUtils() {
    }

    public static HistoryUtils getInstance() {
        HistoryUtils localInstance = instance;
        if (localInstance == null) {
            synchronized (HistoryUtils.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new HistoryUtils();
                }
            }
        }
        return localInstance;
    }

    @Override
    public String getAll() {
        return String.format("SELECT * FROM %s.%s", SCHEMA_NAME, HISTORY_TABLE_NAME);
    }

    @Override
    public PreparedStatement getByKey(Connection connection,
                                      String keyName,
                                      int key) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s=?",
                SCHEMA_NAME,
                HISTORY_TABLE_NAME,
                keyName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, key);
        return statement;
    }

    @Override
    public PreparedStatement getByValue(Connection connection,
                                        String valueName,
                                        String value) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s=?",
                SCHEMA_NAME,
                HISTORY_TABLE_NAME,
                valueName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, value);
        return statement;
    }

    @Override
    public PreparedStatement insert(Connection connection, History entity) throws SQLException {
        String query = String.format("INSERT INTO %s.%s " +
                        "(%s, %s, %s, %s, %s) " +
                        "VALUES (?,?,?,?,?)",
                // INSERT ----------------
                SCHEMA_NAME,
                HISTORY_TABLE_NAME,
                // COLUMN NAMES ----------
                HISTORY_BOOK_ID_COLUMN_NAME,
                HISTORY_USER_ID_COLUMN_NAME,
                HISTORY_DATE_OF_ISSUE_COLUMN_NAME,
                HISTORY_RETURN_DATE_COLUMN_NAME,
                HISTORY_INFO_COLUMN_NAME);
        PreparedStatement statement =
                connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, entity.getBookId());
        statement.setInt(2, entity.getUserId());
        statement.setTimestamp(3, entity.getDateOfIssue());
        statement.setTimestamp(4, entity.getReturnDate());
        statement.setString(5, entity.getInfo());
        return statement;
    }

    @Override
    public PreparedStatement update(Connection connection, History entity) throws SQLException {
        String query =
                String.format("UPDATE %s.%s SET " +
                                //BookID UserID
                                "%s= ?, %s= ?, " +
                                //dateOfIssue   returnDate
                                "%s= ?, %s= ?, " +
                                //Info
                                "%s= ? " +
                                "WHERE id = ?;",
                        //UPDATE -----------------
                        SCHEMA_NAME,
                        HISTORY_TABLE_NAME,

                        //SET --------------------
                        HISTORY_BOOK_ID_COLUMN_NAME,
                        HISTORY_USER_ID_COLUMN_NAME,

                        HISTORY_DATE_OF_ISSUE_COLUMN_NAME,
                        HISTORY_RETURN_DATE_COLUMN_NAME,

                        HISTORY_INFO_COLUMN_NAME);

        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, entity.getBookId());
        statement.setInt(2, entity.getUserId());

        statement.setTimestamp(3, entity.getDateOfIssue());
        statement.setTimestamp(4, entity.getReturnDate());

        statement.setString(5, entity.getInfo());

        statement.setInt(6, entity.getId());
        return statement;

    }

    @Override
    public PreparedStatement delete(Connection connection, int id) throws SQLException {
        String query = String.format("DELETE FROM %s.%s WHERE %s = ?;",
                // DELETE  -----------
                SCHEMA_NAME,
                HISTORY_TABLE_NAME,
                // WHERE -------------
                HISTORY_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        return statement;
    }

    /**
     * Checking for three things: bookID, userId and dateOfIssue
     * @param connection
     * @param entity entity
     * @return
     * @throws SQLException
     */
    @Override
    public PreparedStatement isExist(Connection connection, History entity) throws SQLException {
        String query =
                String.format("SELECT * FROM %s.%s WHERE %s=? AND %s=? AND %s=?;",
                        // SELECT --------------
                        SCHEMA_NAME,
                        HISTORY_TABLE_NAME,
                        // WHERE ---------------
                        HISTORY_BOOK_ID_COLUMN_NAME,
                        HISTORY_USER_ID_COLUMN_NAME,
                        HISTORY_DATE_OF_ISSUE_COLUMN_NAME);

        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, entity.getBookId());
        statement.setInt(2, entity.getUserId());
        statement.setTimestamp(3, entity.getDateOfIssue());
        return statement;
    }

    @Override
    public History getFromResultSet(ResultSet resultSet) throws SQLException {
        return new History(
                resultSet.getInt(HISTORY_ID_COLUMN_NAME),
                resultSet.getInt(HISTORY_BOOK_ID_COLUMN_NAME),
                resultSet.getInt(HISTORY_USER_ID_COLUMN_NAME),
                resultSet.getTimestamp(HISTORY_DATE_OF_ISSUE_COLUMN_NAME),

                resultSet.getTimestamp(HISTORY_RETURN_DATE_COLUMN_NAME),
                resultSet.getString(HISTORY_INFO_COLUMN_NAME));
    }
}
