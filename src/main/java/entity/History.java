package entity;

import entity.abstraction.Entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class History implements Serializable, Entity {

    private int id;
    private int bookId;
    private int userId;
    private Timestamp dateOfIssue;
    private Timestamp returnDate;
    private String info;
    private Book book;
    private User user;

    public History() {
    }

    public History(int id, int bookId, int userId, Timestamp dateOfIssue, Timestamp returnDate, String info) {
        this.id = id;
        this.bookId = bookId;
        this.userId = userId;
        this.dateOfIssue = dateOfIssue;
        this.returnDate = returnDate;
        this.info = info;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Timestamp getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(Timestamp dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public Timestamp getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Timestamp returnDate) {
        this.returnDate = returnDate;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        History history = (History) o;

        if (id != history.id) return false;
        if (bookId != history.bookId) return false;
        if (userId != history.userId) return false;
        if (dateOfIssue != null ? !dateOfIssue.equals(history.dateOfIssue) : history.dateOfIssue != null)
            return false;
        if (returnDate != null ? !returnDate.equals(history.returnDate) : history.returnDate != null)
            return false;
        return info != null ? info.equals(history.info) : history.info == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + bookId;
        result = 31 * result + userId;
        result = 31 * result + (dateOfIssue != null ? dateOfIssue.hashCode() : 0);
        result = 31 * result + (returnDate != null ? returnDate.hashCode() : 0);
        result = 31 * result + (info != null ? info.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", bookId=" + bookId +
                ", userId=" + userId +
                ", dateOfIssue=" + dateOfIssue +
                ", returnDate=" + returnDate +
                ", info='" + info + '\'' +
                '}';
    }
}
