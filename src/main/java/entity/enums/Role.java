package entity.enums;

import java.io.Serializable;

public enum Role implements Serializable {

    ADMIN(1), LIBRARIAN(2), READER(3);

    final int value;

    private Role(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public static Role ofStatusCode(int value) {

        switch (value) {

            case 0:
                return ADMIN;

            case 1:
                return LIBRARIAN;

            case 2:
                return READER;

            default:
                return null;

        }
    }

    public static Role ofString(String role) {
        switch (role) {

            case "admin":
                return ADMIN;

            case "librarian":
                return LIBRARIAN;

            case "reader":
                return READER;

            default:
                return null;

        }
    }
}

