package entity;

import entity.abstraction.Entity;

import java.io.Serializable;
import java.sql.Date;

public class Book implements Serializable, Entity {

    private int id;
    private String name;
    private String info;
    private String genre;
    private String authorName;
    private String authorSurname;
    private String authorPatronymic;
    private Date yearOfEdition;
    private int amount;
    private int issued;
    private String img_href;

    public Book() {
    }

    /**
     * TODO ENG
     * Конструктор для всех параметров
     *
     * @param id
     * @param name
     * @param info
     * @param genre
     * @param authorName
     * @param authorSurname
     * @param authorPatronymic
     * @param yearOfEdition
     * @param amount
     * @param issued
     * @param img_href
     */
    public Book(int id, String name, String info, String genre, String authorName, String authorSurname, String authorPatronymic, Date yearOfEdition, int amount, int issued, String img_href) {
        this.id = id;
        this.name = name;
        this.info = info;
        this.genre = genre;
        this.authorName = authorName;
        this.authorSurname = authorSurname;
        this.authorPatronymic = authorPatronymic;
        this.yearOfEdition = yearOfEdition;
        this.amount = amount;
        this.issued = issued;
        this.img_href = img_href;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorSurname() {
        return authorSurname;
    }

    public void setAuthorSurname(String authorSurname) {
        this.authorSurname = authorSurname;
    }

    public String getAuthorPatronymic() {
        return authorPatronymic;
    }

    public void setAuthorPatronymic(String authorPatronymic) {
        this.authorPatronymic = authorPatronymic;
    }

    public Date getYearOfEdition() {
        return yearOfEdition;
    }

    public void setYearOfEdition(Date yearOfEdition) {
        this.yearOfEdition = yearOfEdition;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getIssued() {
        return issued;
    }

    public void setIssued(int issued) {
        this.issued = issued;
    }

    public String getImg_href() {
        return img_href;
    }

    public void setImg_href(String img_href) {
        this.img_href = img_href;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (id != book.id) return false;
        if (amount != book.amount) return false;
        if (issued != book.issued) return false;
        if (name != null ? !name.equals(book.name) : book.name != null) return false;
        if (info != null ? !info.equals(book.info) : book.info != null) return false;
        if (genre != null ? !genre.equals(book.genre) : book.genre != null) return false;
        if (authorName != null ? !authorName.equals(book.authorName) : book.authorName != null)
            return false;
        if (authorSurname != null ? !authorSurname.equals(book.authorSurname) : book.authorSurname != null)
            return false;
        if (authorPatronymic != null ? !authorPatronymic.equals(book.authorPatronymic) : book.authorPatronymic != null)
            return false;
        if (yearOfEdition != null ? !yearOfEdition.equals(book.yearOfEdition) : book.yearOfEdition != null)
            return false;
        return img_href != null ? img_href.equals(book.img_href) : book.img_href == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (authorSurname != null ? authorSurname.hashCode() : 0);
        result = 31 * result + (authorPatronymic != null ? authorPatronymic.hashCode() : 0);
        result = 31 * result + (yearOfEdition != null ? yearOfEdition.hashCode() : 0);
        result = 31 * result + amount;
        result = 31 * result + issued;
        result = 31 * result + (img_href != null ? img_href.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", info='" + info + '\'' +
                ", genre='" + genre + '\'' +
                ", authorName='" + authorName + '\'' +
                ", authorSurname='" + authorSurname + '\'' +
                ", authorPatronymic='" + authorPatronymic + '\'' +
                ", yearOfEdition=" + yearOfEdition +
                ", amount=" + amount +
                ", issued=" + issued +
                ", img_href='" + img_href + '\'' +
                '}';
    }
}
