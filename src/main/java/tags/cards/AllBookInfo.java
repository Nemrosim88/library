package tags.cards;

import control.controller.Controller;
import data.Constants;
import data.dao.implementation.EntityDAO;
import data.dao.implementation.utils.BookUtils;
import entity.*;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;

public class AllBookInfo extends BodyTagSupport implements Constants {

    private static final Logger logger = Logger.getLogger(AllBookInfo.class);
    private int bookId;
    private String divClass;

    public String getDivClass() {
        return divClass;
    }

    public void setDivClass(String divClass) {
        this.divClass = divClass;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    @Override
    public int doStartTag() throws JspException {
        logger.info(": doStartTag()");
        try {
            JspWriter out = pageContext.getOut();

            String command = "allBooksInfo";

            Connection connection = Controller.connection;
            logger.info(": connection -> " + connection);

            EntityDAO<Book> dao = new EntityDAO<>(connection, BookUtils.getInstance());

            //Получаем все книги
            List<Book> allBooksList = dao.getAll();


            out.print("<div class=\"album py-5 bg-light\">");
            out.print("<div class=\"container\">");
            out.print("<div class=\"row\">");


            for (Book book : allBooksList) {

                out.print("<div class=\"" + divClass + "\">");
                out.print("<div class=\"card mb-4box-shadow\">");
                out.print("<img class=\"card-img-top\"");

                out.print("src=\"/img/books/" + book.getImg_href() + ".jpg\"");
                out.print(" alt=\"Book cover\"");
                out.print(" style=\"height:30 %; width: 100 %; display: block;\"");
                out.print("data-holder-rendered=\"true\">");
                out.print("<div class=\"card-body\">");
                out.print("<p class=\"card-text\">" + book.getInfo() + "</p>");

                out.print("<div class=\"d-flex justify-content-between align-items-center\">");
                out.print("<div class=\"btn-group\">");
                out.print("<button type=\"button\"");
                out.print("class=\"btn btn-sm btn-outline-secondary\">" + "BOOK ID: " + book.getId() + "</button>");

                out.print("<button type=\"button\"");
                out.print("class=\"btn btn-sm btn-outline-secondary\">" + "Amount: " + book.getAmount() + "</button>");

                out.print("</div>");
                out.print("<small class=\"text-muted\">"+"Issued:"+book.getIssued()+"</small>");
                out.print("</div>");
                out.print("</div>");
                out.print("</div>");
                out.print("</div>");

            }
            out.print("</div>");
            out.print("</div>");
            out.print("</div>");

        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public void setPageContext(PageContext pageContext) {
        super.setPageContext(pageContext);
    }

}
